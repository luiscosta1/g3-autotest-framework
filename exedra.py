####################################################################################
# Filename:     exedra.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Version:      1.0
#
# Filetype:     Library file
# Description:  Functions to support HTTP requests to Exedra Platform
# STATUS:       New
# Limitation:
# TODO:         - Create function to send dimming level -> Check Core APIs "Real Time Control"
#               - Make exedra as a Class (?)
####################################################################################
"""Functions to support HTTP requests to Exedra Platform"""


# import requests
from requests.auth import HTTPBasicAuth
from requests import request
import json
import config
from logs import logger


# https://hyperion.tst-schreder-exedra.com/documentation/core-apis

# Add Exedra Personal Access Token to the Resources_generic

# rework this function to work with Exedra


def exedra_request(url, request_type, payload=''):
    """
    Performs requests to Exedra API
    Returns the response from the server
    """

    headers = {
        "Accept": "application / json, text / plain, * / *",
        "Authorization": "Bearer " + config.exedra_token
    }

    query = {
        'query': 'query'
    }

    response = request(
        request_type,
        url,
        headers=headers,
        # params=query
    )

    return response


def exedra_request_device_id(device_id=config.DeviceID) -> bool:
    """
    Requests a search for a specific Device ID in Exedra.
    Returns True if the device is found
    """
    # Exedra API returns the Exedra device ID, e.g. "id": "6ec47f28-2cba-4291-98ed-c8b3f74d38fd"
    # url = 'https://' + exedra_url + '/api/v2/devices/'

    # What we want, though, is a request by Our Device ID, e.g. 0013A20041BCA3FA
    # This is possible by using the Inventory List HTTP Request
    url = 'https://' + config.exedra_url + '/api/v1/devices?queryLang=ObjectName%20%3D%20'

    # Append the desired device_id to the end of the URL requested
    url += device_id

    logger.debug(f'Requesting {device_id} to Exedra')

    response = exedra_request(url, 'GET')

    # check if device ID is present in response
    if device_id not in response.text:
        logger.warning(f'Device {device_id} not found in Exedra')
        return False

    return True


def exedra_request_device_properties(device_id, parameter) -> str:
    """
    Requests a search for a specific Device ID in Exedra.
    Returns the desired parameter about the device, e.g. Firmware version. dimmingType, etc.
    """

    url = 'https://' + config.exedra_url + '/api/v1/devices?queryLang=ObjectName%20%3D%20'

    # Append the desired device_id to the end of the URL requested
    url += device_id

    logger.debug(f'Requesting {device_id} Properties to Exedra')

    response = exedra_request(url, 'GET')

    if device_id not in response.text:
        logger.warning(f'Device {device_id} not found in Exedra')
        return ''

    response = get_value_from_exedra_response(response, parameter)

    return response


def get_value_from_exedra_response(response, parameter) -> str:
    """
    Receives the data from a device in Json format
    Returns the desired parameter defined in "value"
    """

    # Use Json library to convert the response
    data_list = json.loads(response.text)  # data_dict is created as a Python dictionary from the Json text

    """
    data_list is a list of several dictionaries, with different parameters, such as:
    'core', 'coords', 'attributes', 'failures', 'configuration', 'metering'

    we need to check in which dictionary the requested parameter is located

    data_list has strings, integers and dictionaries. We only need the dictionaries in this context
    """

    list_dicts = [data_list[0]['core'], data_list[0]['coords'], data_list[0]['attributes'], data_list[0]['failures'],
                  data_list[0]['configuration'], data_list[0]['metering']]

    for dictionary in list_dicts:  # for every dictionary present in list_dicts
        parameter_value = dictionary.get(parameter)
        if parameter_value is None:
            continue
        else:
            # Parameter found!
            break

    return parameter_value



