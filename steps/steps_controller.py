from behave import given, when, then, step
from g3connect.g3driver import G3Driver
import g3_d2c_messages as d2c
import config
import imperium
from time import sleep
from logs import logger


#################################
#   Given Steps Implementation  #
#################################

@given("the device is reachable via CLI")
def step_impl(context):

    logger.info(f"Trying to connect to: {context.DeviceID} ...")
    # Need to receive Device ID from above context.DeviceID

    # create g3 instance and save it in the context
    context.g3 = G3Driver(config.g3_connection_settings)

    if context.g3.open_g3_cli(context.DeviceID):
        logger.info(f"Connect to Device: {context.DeviceID} - OK")
        return True
    else:
        # No need to disconnect from device, because that is taken care of inside open_g3_cli()
        assert False, f"Connect to Device: {context.DeviceID} - NOK"


@given('the device is written the "{write_parameter}"')
def step_impl(context, write_parameter):

    if write_parameter == "Time":
        if context.g3.set_g3_time(config.now_UTC()):
            logger.info(f"Write {write_parameter} to Device - OK")
            return True
        else:
            assert False, f"Write {write_parameter} to Device - NOK"

    elif write_parameter == "Registration Server APNs":
        if context.g3.write_g3_coap("exedra_reg_server_no_trig", context.DeviceID):
            logger.info(f"Write {write_parameter} to Device - OK")
            return True
        else:
            assert False, f"Write {write_parameter} to Device - NOK"

    elif write_parameter == "GPS coordinates":
        if context.g3.set_g3_gps(config.settings["gps_lat"], config.settings["gps_long"]):
            logger.info(f"Write {write_parameter} to Device - OK")
            return True
        else:
            assert False, f"Write {write_parameter} to Device - NOK"

    elif write_parameter == "Update Server APNs":
        if context.g3.write_g3_coap("exedra_upd_server", context.DeviceID):
            logger.info(f"Write {write_parameter} to Device - OK")
            return True
        else:
            logger.error()
            assert False, f"Write {write_parameter} to Device - NOK"

    elif write_parameter == "Update Server Notification APNs":
        if context.g3.write_g3_coap("exedra_upd_notif_server", context.DeviceID):
            logger.info(f"Write {write_parameter} to Device - OK")
            return True
        else:
            assert False, f"Write {write_parameter} to Device - NOK"

    else:
        assert False, f"Unknown Parameter {write_parameter} to write on Device - NOK"


@given('the device reports a valid "{read_parameter}"')
def step_impl(context, read_parameter):

    if read_parameter == 'Asset ID':
        # Read Asset ID and save to context
        context.asset_id = context.g3.read_datapoint("RFIDID")
        if len(context.asset_id) == 16 and context.asset_id.isalnum():  # Length Must be 16 and Alphanumerical
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Asset ID:  {context.asset_id}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"

    elif read_parameter == 'Firmware Version':
        # Read Device Firmware and save to context
        context.firmware_initial = context.g3.read_datapoint("FW-LUCO")
        if len(context.firmware_initial) != 0:  # Some FW version was read
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Device Firmware Version:  {context.firmware_initial}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"

    elif read_parameter == 'Dimming Level':
        # Read Device Dimming Level and save to context
        context.dim_level_initial = int(float(context.g3.read_datapoint("FeedbackDIMLevel")))
        if 0 <= context.dim_level_initial <= 100:
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Device Dimming Level:  {context.dim_level_initial}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"

    elif read_parameter == 'Active power':
        # Read Device Active power and save to context
        context.active_power = float(context.g3.read_datapoint("ACPower"))
        if context.active_power > 0:
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Device Active Power:  {context.active_power}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"

    elif read_parameter == 'Power factor':
        # Read Device Power factor and save to context
        context.power_factor = float(context.g3.read_datapoint("ACPF"))
        if 0 <= context.power_factor <= 1:
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Device Power factor:  {context.power_factor}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"

    elif read_parameter == 'RMS voltage':
        # Read Device RMS voltage and save to context
        context.rms_voltage = float(context.g3.read_datapoint("ACVolt"))
        if 100 <= context.rms_voltage <= 350:
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Device RMS voltage:  {context.rms_voltage}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"

    elif read_parameter == 'RMS current':
        # Read Device RMS current and save to context
        context.rms_current = float(context.g3.read_datapoint("ACCurrent"))
        if 100 <= context.rms_current <= 350:
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Device RMS current:  {context.rms_current}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"

    elif read_parameter == 'Controller temperature':
        # Read Device Temperature and save to context
        context.temperature = float(context.g3.read_datapoint("CurrentTemp"))
        if -20 <= context.temperature <= 150:
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Device Temperature:  {context.temperature}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"

    elif read_parameter == 'Cumulative lamp burning hours':
        # Read Device Lamp Burning hours and save to context
        context.burning_hours = float(context.g3.read_datapoint("TotalRuntime", instance=1))
        if context.burning_hours >= 0:
            logger.info(f"Read {read_parameter} from Device - OK")
            logger.info(f"Device lamp burning hours:  {context.burning_hours}")
            return True
        else:
            assert False, f"Read {read_parameter} from Device - NOK"
    else:
        assert False, f"Unknown Parameter {read_parameter} to read from the Device - NOK"


################################
#   When Steps Implementation  #
################################


@when("the device starts registering in Exedra")
def step_impl(context):

    #logger.warning('Step Disabled - Device will Not Re-Register!')

    if context.g3.write_g3_coap("re_register", context.DeviceID):
        sleep(5)
        device_registration_state = int(context.g3.read_datapoint("RegnState"))
        if device_registration_state == 0:
            logger.info('Write "Re-Register" to Device - OK')
            return True
        else:
            # logger.error(f'Write "Re-Register" to Device {device_registration_state=} - NOK')
            assert False, f'Write "Re-Register" to Device {device_registration_state=} - NOK'
    else:
        assert False, 'Write "Re-Register" to Device - NOK'


@when('the Device "{action}" the Alarm "{alarm}"')
def step_impl(context, action, alarm):

    is_alarm_active = True
    if action == "triggers":
        is_alarm_active = True
    elif action == "clears":
        is_alarm_active = False
    else:
        logger.error(f"Unknown Alarm Action: {action}")

    queue_list = []
    if alarm == "Lamp Failure":
        config.BrokenLamp = is_alarm_active
        queue_list = ["BrokenLamp_1"]

    elif alarm == "Lamp power is greater than expected":
        config.ACPower_errors_dict["ErrPWRHigh"] = is_alarm_active
        queue_list = ["ErrPWRHigh"]

    elif alarm == "Lamp power is smaller than expected":
        config.ACPower_errors_dict["ErrPWRLow"] = is_alarm_active
        queue_list = ["ErrPWRLow"]

    elif alarm == "Power factor is below the threshold":
        config.ACPower_errors_dict["ErrPWRPF"] = is_alarm_active
        queue_list = ["ErrPWRPF"]

    elif alarm == "Supply voltage is above the threshold":
        config.ACPower_errors_dict["ErrVoltageHigh"] = is_alarm_active
        queue_list = ["ErrVoltageHigh"]

    elif alarm == "Supply voltage is below the threshold":
        config.ACPower_errors_dict["ErrVoltageLow"] = is_alarm_active
        queue_list = ["ErrVoltageLow"]

    elif alarm == "Controller Failure":
        config.ErrIPCComm = is_alarm_active
        queue_list = ["ErrIPCComm"]

    elif alarm == "DALI bus short circuit":
        if is_alarm_active:
            config.DALIStatus = "BusShort"
        else:
            config.DALIStatus = ""
        queue_list = ["DALIStatus"]

    elif alarm == "Temperature is above the threshold":
        config.ErrTemperatureHigh = is_alarm_active
        queue_list = ["ErrTemperatureHigh"]

    elif alarm == "DALI communication failure":
        if is_alarm_active:
            config.DALIBallastStatus = "CommsError"
        else:
            config.DALIBallastStatus = "LampArcPowerOn"
        queue_list = ["DALIBallastStatus_1"]

    elif alarm == "Luminaire asset information missing":
        logger.error(f"Unknown/Not Implemented Alarm: {alarm}")

    elif alarm == "Invalid RFID read":
        logger.error(f"Unknown/Not Implemented Alarm: {alarm}")

    else:
        logger.error(f"Unknown/Not Implemented Alarm: {alarm}")

    # build the queue
    queue = d2c.build_g3_d2c_queue(queue_list, context.DeviceID)
    # build the payload
    message_body = d2c.build_g3_d2c_message(queue, context.DeviceID)

    # send the request
    logger.debug(f'Sending Device 2 Cloud Message with: {str(queue_list)}')

    response = d2c.send_d2c_exedra_coap(message_body)

    if response is None:
        logger.error("Message not Acknowledged from Exedra. Is the Host whitelisted in TST G3 Gateway?")
        return False
    elif "ACK" not in response.line_print:
        logger.error(f"Message not Acknowledged from Exedra:{response}. Is the Host whitelisted in TST G3 Gateway?")
        return False

    logger.info(f'COAP message with {alarm} sent - OK')


@when('the Device reports the Telemetry "{telemetry}" as "{value}"')
def step_impl(context, telemetry, value):

    if telemetry == "Cumulative active energy":
        config.dimfeedback_dict["EnergyMeter"] = str(value)
    elif telemetry == "Active power":
        config.dimfeedback_dict["ACPower"] = str(value)
    elif telemetry == "RMS current":
        config.dimfeedback_dict["ACCurrent"] = str(value)
    elif telemetry == "RMS voltage":
        config.dimfeedback_dict["ACVoltage"] = str(value)
    elif telemetry == "Power factor":
        config.dimfeedback_dict["ACPowerFactor"] = str(value)
    elif telemetry == "Lamp level":
        config.dimfeedback_dict["FeedbackDIMLevel"] = str(value)
    elif telemetry == "Cumulative lamp burning hours":
        config.BurningHours_dict["TotalRuntime"] = str(value)

    else:
        logger.error(f"Unknown/Not Implemented Telemetry: {telemetry}")

    if telemetry == "Cumulative lamp burning hours":
        queue_list = ["BurningHours"]
    else:
        queue_list = ["Dimfeedback_1"]
    # build the queue
    queue = d2c.build_g3_d2c_queue(queue_list, context.DeviceID)
    # build the payload
    message_body = d2c.build_g3_d2c_message(queue, context.DeviceID)

    # send the request
    logger.debug(f'Sending Device 2 Cloud Message with: {str(queue_list)}')

    response = d2c.send_d2c_exedra_coap(message_body)

    if response is None:
        logger.error("Message not Acknowledged from Exedra. Is the Host whitelisted in TST G3 Gateway?")
        return False
    elif "ACK" not in response.line_print:
        logger.error(f"Message not Acknowledged from Exedra:{response}. Is the Host whitelisted in TST G3 Gateway?")
        return False

    logger.info(f'COAP message with {telemetry} sent - OK')


@when('the device is written the "{write_parameter}" to "{value}"')
def step_impl(context, write_parameter, value):

    coap_parameter = ""
    if write_parameter == "High Active power threshold":
        # need to change dimming curve parameters before sending it:
        if value == "higher value":
            # config.dimcurve_dict["LowWattMinDim"] = str((context.active_power * 0.2)).format("0:.2f")
            # config.dimcurve_dict["HighWattMinDim"] = str((context.active_power * 1.2)).format("0:.2f")
            # # Set the Low at High and High at High Thresholds to higher values, to Clear the Power High Alarm
            # config.dimcurve_dict["LowWattMaxDim"] = str((context.active_power * 0.8)).format("0:.2f")
            # config.dimcurve_dict["HighWattMaxDim"] = str((context.active_power * 1.4)).format("0:.2f")
            config.dimcurve_dict["LowWattMinDim"] = "0"
            config.dimcurve_dict["HighWattMinDim"] = "50"
            # Set the Low at High and High at High Thresholds to higher values, to Clear the Power High Alarm
            config.dimcurve_dict["LowWattMaxDim"] = "1"
            config.dimcurve_dict["HighWattMaxDim"] = "200"

        elif value == "lower value":
            config.dimcurve_dict["LowWattMinDim"] = str((context.active_power * 0.1)).format("0:.2f")
            config.dimcurve_dict["HighWattMinDim"] = str((context.active_power * 0.2)).format("0:.2f")
            # Set the Low at High and High at High Thresholds to Lower values, to Trigger the Power High Alarm
            config.dimcurve_dict["LowWattMaxDim"] = str((context.active_power * 0.3)).format("0:.2f")
            config.dimcurve_dict["HighWattMaxDim"] = str((context.active_power * 0.4)).format("0:.2f")
        else:
            assert False, f"Unknown value  {value} to write to {write_parameter} - NOK"

        # Send Dimming Curve
        if context.luminaire_type == "1-10V":
            coap_parameter = "1_10V_dim_curve"
        else:
            coap_parameter = "DALI_dim_curve"

    elif write_parameter == "Low Active power threshold":
        # need to change dimming curve parameters before sending it:
        if value == "higher value":
            # Set the Low at Low and High at Low Thresholds to higher values, to Trigger the Power Low Alarm
            config.dimcurve_dict["LowWattMinDim"] = str((context.active_power * 1.2)).format("0:.2f")
            config.dimcurve_dict["HighWattMinDim"] = str((context.active_power * 1.4)).format("0:.2f")
            config.dimcurve_dict["LowWattMaxDim"] = str((context.active_power * 1.5)).format("0:.2f")
            config.dimcurve_dict["HighWattMaxDim"] = str((context.active_power * 1.6)).format("0:.2f")

        elif value == "lower value":
            # Set the Low at Low and High at Low Thresholds to Lower values, to Clear the Power Low Alarm
            # config.dimcurve_dict["LowWattMinDim"] = str((context.active_power * 0.1)).format("0:.2f")
            # config.dimcurve_dict["HighWattMinDim"] = str((context.active_power * 1.2)).format("0:.2f")
            # config.dimcurve_dict["LowWattMaxDim"] = str((context.active_power * 0.3)).format("0:.2f")
            # config.dimcurve_dict["HighWattMaxDim"] = str((context.active_power * 1.4)).format("0:.2f")
            # Set the Low at Low and High at Low Thresholds to Lower values, to Clear the Power Low Alarm
            config.dimcurve_dict["LowWattMinDim"] = "0"
            config.dimcurve_dict["HighWattMinDim"] = "50"
            config.dimcurve_dict["LowWattMaxDim"] = "1"
            config.dimcurve_dict["HighWattMaxDim"] = "200"
        else:
            assert False, f"Unknown value  {value} to write to {write_parameter} - NOK"

        # Send Dimming Curve
        if context.luminaire_type == "1-10V":
            coap_parameter = "1_10V_dim_curve"
        else:
            coap_parameter = "DALI_dim_curve"

    else:
        assert False, f"Unknown Parameter {write_parameter} to write on Device - NOK"

    # Send COAP message via CLI:
    if context.g3.write_g3_coap(coap_parameter, context.DeviceID):
        logger.info(f"Write {write_parameter} to {value} on Device via CLI - OK")
        return True
    else:
        assert False, f"Failed to write {write_parameter} to {value} on Device via CLI - NOK"


################################
#   Then Steps Implementation  #
################################

@then('the device is disconnected from the CLI')
def step_impl(context):
    try:
        # Verify if the connection has already been made.
        if context.g3:
            logger.info(f"Disconnecting from: {context.DeviceID}")
            context.g3.close_g3_cli()
            return True
    except AttributeError:
        logger.warning(f"Tester was not connected to: {context.DeviceID} ...")
        return True


@then('the device shall receive the "{read_parameter}"')
def step_impl(context, read_parameter):

    if read_parameter == 'Project Server APNs':
        apn_url = context.g3.read_datapoint("APN")
        apn_usr = context.g3.read_datapoint("APNUser")
        apn_pwd = context.g3.read_datapoint("APNPW")
        surl = context.g3.read_datapoint("SURL")
        suser = context.g3.read_datapoint("Suser")
        spwd = context.g3.read_datapoint("Spw")

        assert apn_url in config.EXEDRA_APN_URL
        assert apn_usr in config.EXEDRA_APN_USR
        assert apn_pwd in config.EXEDRA_APN_PWD
        assert config.exedra_Surl in surl
        assert suser in ""
        assert spwd in ""

        logger.info(f'Device Receiving {read_parameter} - OK')

    elif read_parameter == 'Asset Dimming Curve':

        # Get Dimming Curve(s) from Imperium
        dimming_curve = imperium.imperium_request_dimming_curve(context.asset_id)
        if dimming_curve is None:
            assert False, f"Asset {context.asset_id} has no Dimming Curve! - NOK"

        tenant_configs = imperium.imperium_request_tenant_properties(config.tenant)
        if tenant_configs is None:
            assert False, f"Tenant {config.tenant} Not Found! - NOK"

        # Read the configurations from the Controller
        low_at_low = float(context.g3.read_datapoint("LowWattMinDim"))
        low_at_max = float(context.g3.read_datapoint("LowWattMaxDim"))
        high_at_low = float(context.g3.read_datapoint("HighWattMinDim"))
        high_at_max = float(context.g3.read_datapoint("HighWattMaxDim"))
        power_factor_th = float(context.g3.read_datapoint("PowerFactorLimit"))
        lamp_type = context.g3.read_datapoint("LampTypeIP")    # "DALI" or "1-10V"
        lamp_indexes = int(context.g3.read_datapoint("DALIindex"))  # NO VALUE for 1-10V


        # All Parameters must match the Dimming Curve from the Asset in Imperium!
        if low_at_low == dimming_curve[0]["lowerLimitMinDim"] \
            and low_at_max == dimming_curve[0]["lowerLimitMaxDim"] \
                    and high_at_low == dimming_curve[0]["highLimitMinDim"] \
                        and high_at_max == dimming_curve[0]["highLimitMaxDim"] \
                            and power_factor_th == tenant_configs["deviceTypes"][0]["thresholds"]["powerFactorLimit"]: # Verify this when tenant is already in context
            logger.info("Dimming curve on the device matches Imperium")
            success = True
        else:
            logger.error('Dimming curve on the device does not match Imperium!!!!!!!!!!!!!!')
            logger.error(f'Device {low_at_low=} vs Asset: {dimming_curve[0]["lowerLimitMinDim"]=}')
            logger.error(f'Device {low_at_max=} vs Asset: {dimming_curve[0]["lowerLimitMaxDim"]=}')
            logger.error(f'Device {high_at_low=} vs Asset: {dimming_curve[0]["highLimitMinDim"]=}')
            logger.error(f'Device {high_at_max=} vs Asset: {dimming_curve[0]["highLimitMaxDim"]=}')
            logger.error(f'Device {power_factor_th=} vs Tenant: {tenant_configs["deviceTypes"][0]["thresholds"]["powerFactorLimit"]=}')
            success = False

        # On 1-10V Luminaires, compare only the LampTypeIP
        if context.luminaire_type == "1-10V":
            if lamp_type == context.luminaire_type:
                success = True
            else:
                logger.error(f' LampTypes dont match - Device: {lamp_type=} vs Desired: {context.luminaire_type=}')
                success = False

        # If not 1-10V, compare the LampTypeIP and the DALIindex
        elif context.luminaire_type == "DALI" \
                    or context.luminaire_type == "Multi-DALI" \
                        or context.luminaire_type == "Tuneable-White":

            if lamp_type == "DALI" and int(lamp_indexes) == len(dimming_curve):
                success = True
            else:
                logger.error('Lamp Type or Indexes dont match:')
                logger.error(f' Lamp Type Device: {lamp_type=} vs Desired: {context.luminaire_type=}')
                logger.error(f' Lamp Indexes Device: {lamp_indexes=} vs Desired: {len(dimming_curve)}')
                success = False
        else:
            # TODO: there's something fishy about the statement below
            success = False
            logger.error(f"Unknown Luminaire Type! {context.luminaire_type}")

        # If all of the above are OK, it means the Asset Configs on the Controller match the desired in Imperium
        if success:
            logger.info(f'Device Receiving {read_parameter} - OK')
        else:
            assert False, f'Device Receiving {read_parameter} - NOK'

    elif read_parameter == 'Tenant Timezone':
        timezone = context.g3.read_datapoint("Timezone")
        if timezone == config.timezone:
            logger.info(f'Device Receiving {read_parameter} - OK')
        else:
            assert False, f'Device Receiving {read_parameter} - NOK'

    elif read_parameter == 'Tenant DayLightSavingDelta':
        dst = context.g3.read_datapoint("DaylightSavingDelta")
        if dst == config.daylight_saving_delta:
            logger.info(f'Device Receiving {read_parameter} - OK')
        else:
            assert False, f'Device Receiving {read_parameter} - NOK'

    elif read_parameter == 'Tenant Default Dimming Profile':
        """  In G3, it's not possible to read the default Switching Profile from a specific datapoint
            We could only do this if the device is sent a factory reset and deleted from the Platform
            However we guarantee the default profile has been sent (ConfID10) to the device
            Because in the end, RegnState == 2 means that it was received"""

        # The Datapoint below shows the last calendar or manual command sent to the device
        tenant_configs = imperium.imperium_request_tenant_properties(config.tenant)
        if tenant_configs is None:
            assert False, f"Tenant {config.tenant} Not Found! - NOK"

        # from tenant
        desired_default_profile = tenant_configs['deviceTypes'][0]['switchingProfiles']
        # from device
        device_switching_profile = context.g3.read_datapoint("LampValidation")
        # logger.info(f"{device_switching_profile=}")
        # logger.info(f"{desired_default_profile=}")
        logger.info(f'Device Receiving {read_parameter} - !!!!STUB!!!!')
        pass

    elif read_parameter == 'Tenant Temperature Configurations':

        tenant_configs = imperium.imperium_request_tenant_properties(config.tenant)
        if tenant_configs is None:
            assert False, f"Tenant {config.tenant} Not Found! - NOK"

        high_temperature_hyst = int(context.g3.read_datapoint("TemperatureTimeHyst"))
        high_temperature_thresh = int(context.g3.read_datapoint("SPHighTemperature"))

        if high_temperature_hyst == tenant_configs["deviceTypes"][0]["thresholds"]["temperature"]["hyst"] \
                and high_temperature_thresh == tenant_configs["deviceTypes"][0]["thresholds"]["temperature"]["high"]:
            logger.info(f'Device Receiving {read_parameter} - OK')
        else:
            logger.error(f' Temp Hyst Device: {high_temperature_hyst=} vs'
                               f' Desired: {tenant_configs["deviceTypes"][0]["thresholds"]["temperature"]["hyst"]}')
            logger.error(f' Temp High Device: {high_temperature_thresh=} vs'
                               f' Desired: {tenant_configs["deviceTypes"][0]["thresholds"]["temperature"]["high"]}')
            assert False, f'Device Receiving {read_parameter} - NOK'

    elif read_parameter == 'Tenant Voltage Configurations':

        tenant_configs = imperium.imperium_request_tenant_properties(config.tenant)
        if tenant_configs is None:
            assert False, f"Tenant {config.tenant} Not Found! - NOK"

        high_voltage_hyst = int(context.g3.read_datapoint("VoltageTimeHyst"))
        high_voltage_thresh = int(context.g3.read_datapoint("SPHighVoltage"))
        low_voltage_thresh = int(context.g3.read_datapoint("SPLowVoltage"))

        if high_voltage_hyst == tenant_configs["deviceTypes"][0]["thresholds"]["voltage"]["hyst"] \
                and high_voltage_thresh == tenant_configs["deviceTypes"][0]["thresholds"]["voltage"]["high"] \
                    and low_voltage_thresh == tenant_configs["deviceTypes"][0]["thresholds"]["voltage"]["low"]:
            logger.info(f'Device Receiving {read_parameter} - OK')
        else:
            logger.error(f' Temp Hyst Device: {high_voltage_hyst=} vs'
                               f' Desired: {tenant_configs["deviceTypes"][0]["thresholds"]["voltage"]["hyst"]}')
            logger.error(f' Temp High Device: {high_voltage_thresh=} vs'
                               f' Desired: {tenant_configs["deviceTypes"][0]["thresholds"]["voltage"]["high"]}')
            logger.error(f' Temp High Device: {low_voltage_thresh=} vs'
                               f' Desired: {tenant_configs["deviceTypes"][0]["thresholds"]["voltage"]["low"]}')
            assert False, f'Device Receiving {read_parameter} - NOK'

    elif read_parameter == 'Tenant Power Factor Configuration':

        tenant_configs = imperium.imperium_request_tenant_properties(config.tenant)
        if tenant_configs is None:
            assert False, f"Tenant {config.tenant} Not Found! - NOK"

        high_voltage_hyst = float(context.g3.read_datapoint("PowerFactorLimit"))

        if high_voltage_hyst == tenant_configs["deviceTypes"][0]["thresholds"]["powerFactorLimit"]:
            logger.info(f'Device Receiving {read_parameter} - OK')
        else:
            logger.error(f' Temp Hyst Device: {high_voltage_hyst=} vs'
                               f' Desired: {tenant_configs["deviceTypes"][0]["thresholds"]["powerFactorLimit"]}')
            assert False, f'Device Receiving {read_parameter} - NOK'

    else:
        assert False, f'Unknown Parameter to read from device {read_parameter} - NOK'


@then('the device shall report the "{read_parameter}" as "{expected_response}"')
def step_impl(context, read_parameter, expected_response):

    if read_parameter == 'Registration Status':
        regn_state = context.g3.read_datapoint("RegnState")

        if regn_state == expected_response:
            logger.info(f'Device Reporting {read_parameter} as {expected_response} - OK')
        else:
            assert False, f'Device Reporting {read_parameter} as {regn_state} != {expected_response} - NOK'

    elif read_parameter == 'Lamp power is greater than expected':
        response = context.g3.read_datapoint("ErrPWRHigh")

        if expected_response == "active":
            assert response == "true",  f'Device Reporting {read_parameter} as {response} != "true" - NOK'
        elif expected_response == "inactive":
            assert response == "false",  f'Device Reporting {read_parameter} as {response} != "false" - NOK'
        else:
            assert False, f'Unknown expected response: {expected_response} - NOK'

    elif read_parameter == 'Lamp power is smaller than expected':
        response = context.g3.read_datapoint("ErrPWRLow")

        if expected_response == "active":
            assert response == "true",  f'Device Reporting {read_parameter} as {response} != "True" - NOK'
        elif expected_response == "inactive":
            assert response == "false",  f'Device Reporting {read_parameter} as {response} != "False" - NOK'
        else:
            assert False, f'Unknown expected response: {expected_response} - NOK'

    elif read_parameter == 'Power factor is below the threshold':
        response = context.g3.read_datapoint("ErrPWRPF")

        if expected_response == "active":
            assert response == "true",  f'Device Reporting {read_parameter} as {response} != "True" - NOK'
        elif expected_response == "inactive":
            assert response == "false",  f'Device Reporting {read_parameter} as {response} != "False" - NOK'
        else:
            assert False, f'Unknown expected response: {expected_response} - NOK'

    elif read_parameter == 'Supply voltage is above the threshold':
        response = context.g3.read_datapoint("ErrVoltageHigh")

        if expected_response == "active":
            assert response == "true",  f'Device Reporting {read_parameter} as {response} != "True" - NOK'
        elif expected_response == "inactive":
            assert response == "false",  f'Device Reporting {read_parameter} as {response} != "False" - NOK'
        else:
            assert False, f'Unknown expected response: {expected_response} - NOK'

    elif read_parameter == 'Supply voltage is below the threshold':
        response = context.g3.read_datapoint("ErrVoltageLow")

        if expected_response == "active":
            assert response == "true",  f'Device Reporting {read_parameter} as {response} != "True" - NOK'
        elif expected_response == "inactive":
            assert response == "false",  f'Device Reporting {read_parameter} as {response} != "False" - NOK'
        else:
            assert False, f'Unknown expected response: {expected_response} - NOK'

    else:
        assert False, f'Unknown Parameter to read from device {read_parameter} - NOK'

    logger.info(f'Device reports "{read_parameter}" as "{expected_response}" - OK')


@then('the device shall report the correct "{read_parameter}"')
def step_impl(context, read_parameter):

    if read_parameter == 'Firmware Version':
        device_firmware = context.g3.read_datapoint("FW-LUCO")

        if device_firmware == context.firmware_initial:
            logger.info(f'Device Reporting the correct Firmware: {device_firmware} - OK')
        else:
            assert False, f'Device Reporting {read_parameter} as {device_firmware} != {context.firmware_final} - NOK'

    elif read_parameter == 'Final Firmware Version':
        device_firmware = context.g3.read_datapoint("FW-LUCO")

        if device_firmware == context.firmware_final:
            logger.info(f'Device Reporting the correct Firmware: {device_firmware} - OK')
        else:
            assert False, f'Device Reporting {read_parameter} as {device_firmware} != {context.firmware_final} - NOK'

    elif read_parameter == 'Dimming Level':
        device_dim_level = int(float(context.g3.read_datapoint("FeedbackDIMLevel")))

        if device_dim_level == context.dim_level_initial:
            logger.info(f'Device Reporting the correct Dimming Level: {device_dim_level} - OK')
        else:
            assert False, f'Device Reporting {read_parameter} as {device_dim_level} != {context.dim_level_initial} - NOK'

    else:
        assert False, f'Unknown Parameter to read from device {read_parameter} - NOK'
