from support.colours_term import Colours


def initialize():
    '''
    Initialize variables to be used across scripts
    '''
    global serial_com
    global x_mod
    global lgs_index_srch
    serial_com = None
    x_mod = None
    lgs_index_srch = 0


def print_passed():
    print(Colours.Foreground.green, "PASSED", Colours.reset)


def print_failed_msg(msg):
    print(Colours.Foreground.red, msg, Colours.reset)


initialize()
