from behave import given, when, then
import config
from time import sleep
from logs import logger

#################################
#   Given Steps Implementation  #
#################################


@given('the luminaire is of type "{luminaire_type}"')
def step_impl(context, luminaire_type):

    # Save the luminaire_type for further usage in the following steps
    context.luminaire_type = luminaire_type

    # Select the device ID which is assigned to the given luminaire_type
    # By assigning it to context.DeviceID, makes it usable in the following steps
    try:
        context.DeviceID = config.settings[f"{luminaire_type}"]
        logger.info(f"Using DeviceID: {context.DeviceID} which has a Luminaire Type: {luminaire_type}")
    except KeyError:
        logger.error(f"Invalid Luminaire Type: {luminaire_type}")
        assert False

    # Need to validate the DeviceID
    if len(context.DeviceID) != 16:  # Length Must be 16
        logger.error(f'Invalid Device ID length: {context.DeviceID}')
        assert False
    elif not context.DeviceID.isalnum():  # Must be Alphanumerical
        logger.error(f'Invalid Device ID string: {context.DeviceID}')
        assert False
    else:
        # DeviceID is Valid
        return True


################################
#   When Steps Implementation  #
################################


################################
#   Then Steps Implementation  #
################################

@then('the tester shall wait "{time_to_wait}"')
def step_impl(context, time_to_wait):
    """
    time_to_wait Examples:
        * 10 seconds
        * 20 minutes
        * 1 hours
    """

    number = [int(i) for i in time_to_wait.split() if i.isdigit()]

    logger.info(f'Awaiting {time_to_wait} .....')

    if "second" in time_to_wait.lower():
        sleep(number[0])
    elif "minute" in time_to_wait.lower():
        sleep(number[0]*60)
    elif "hour" in time_to_wait.lower():
        sleep(number[0]*3600)
    else:
        logger.error(f'Unknown time to wait: {time_to_wait} - NOK')
        assert False

