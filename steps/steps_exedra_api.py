from behave import given, when, then
import config
import exedra
from logs import logger


#################################
#   Given Steps Implementation  #
#################################
@given('Exedra reports a valid "{read_parameter}"')
def step_impl(context, read_parameter):

    # Only move forward in the device exists in Exedra
    if exedra.exedra_request_device_id(context.DeviceID):

        if read_parameter == 'Firmware Version':

            context.firmware_initial = exedra.exedra_request_device_properties(context.DeviceID, "firmwareVersion")
            if len(context.firmware_initial) != 0:  # Some FW version was read
                logger.info(f"Read {read_parameter} from Exedra - OK")
                logger.info(f"Device Firmware Version:  {context.firmware_initial}")
                return True
            else:
                assert False, f"Read {read_parameter} from Exedra - NOK"

        elif read_parameter == '':
            pass
        else:
            assert False, f'Unknown Parameter {read_parameter} to read from Exedra - NOK'

    else:
        assert False, f'Device {context.DeviceID} not in Exedra - NOK'

################################
#   When Steps Implementation  #
################################


################################
#   Then Steps Implementation  #
################################

@then("Exedra shall report the device as Present")
def step_impl(context):

    if exedra.exedra_request_device_id(context.DeviceID):
        logger.info(f'Device {context.DeviceID} in Exedra - OK')
    else:
        assert False, f'Device {context.DeviceID} not in Exedra - NOK'


@then('Exedra shall report the correct "{read_parameter}"')
def step_impl(context, read_parameter):

    # Only move forward in the device exists in Exedra
    if exedra.exedra_request_device_id(context.DeviceID):

        if read_parameter == 'GPS Coordinates':
            exedra_gps = exedra.exedra_request_device_properties(context.DeviceID, "coordinates")
            exedra_lat = exedra_gps[1]
            exedra_long = exedra_gps[0]

            if exedra_lat == float(config.gps_lat) \
                    and exedra_long == float(config.gps_long):
                logger.info(f'Exedra has the correct {read_parameter} - OK')
            else:
                logger.error(f'Exedra: {exedra_lat=} vs Desired: {config.gps_lat=}')
                logger.error(f'Exedra: {exedra_long=} vs Desired: {config.gps_long=}')
                assert False, f'{read_parameter} Mismatch in Exedra - NOK'

            return True

        elif read_parameter == 'Tenant':
            if config.tenant in exedra.exedra_request_device_properties(context.DeviceID, "groupPath"):
                logger.info(f'Exedra has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Exedra - NOK'

        elif read_parameter == 'Asset ID':
            if context.asset_id == exedra.exedra_request_device_properties(context.DeviceID, "assetId"):
                logger.info(f'Exedra has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Imperium - NOK'

        elif read_parameter == 'Firmware Version':
            if context.firmware_initial == exedra.exedra_request_device_properties(context.DeviceID, "firmwareVersion"):
                logger.info(f'Exedra has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Exedra - NOK'

        elif read_parameter == 'Final Firmware Version':
            if context.firmware_final == exedra.exedra_request_device_properties(context.DeviceID, "firmwareVersion"):
                logger.info(f'Exedra has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Exedra - NOK'

        elif read_parameter == 'Dimming Level':
            if context.dim_level_initial == exedra.exedra_request_device_properties(context.DeviceID, "actualLightState")["value"]:
                logger.info(f'Exedra has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Exedra - NOK'
        else:
            assert False, f'Unknown Parameter {read_parameter} to read from Exedra - NOK'

    else:
        assert False, f'Device {context.DeviceID} not in Exedra - NOK'


@then('Exedra shall report the "{read_parameter}" as "{expected_response}"')
def step_impl(context, read_parameter, expected_response):

    # Only move forward in the device exists in Exedra
    if exedra.exedra_request_device_id(context.DeviceID):

        if read_parameter == 'Lamp level':
            if int(expected_response) == exedra.exedra_request_device_properties(context.DeviceID, "actualLightState")["value"]:
                logger.info(f'Exedra has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Exedra - NOK'

        if read_parameter == 'Cumulative active energy':

            assert float(expected_response) == exedra.exedra_request_device_properties(context.DeviceID, "activeEnergy")["value"],\
                f'{read_parameter} Mismatch in Exedra - NOK'

        if read_parameter == 'Active power':

            assert float(expected_response) == exedra.exedra_request_device_properties(context.DeviceID, "activePower")["value"], \
                f'{read_parameter} Mismatch in Exedra - NOK'

        if read_parameter == 'Power factor':

            assert float(expected_response) == exedra.exedra_request_device_properties(context.DeviceID, "powerFactor")["value"], \
                f'{read_parameter} Mismatch in Exedra - NOK'

        if read_parameter == 'RMS current':

            assert float(expected_response) == exedra.exedra_request_device_properties(context.DeviceID, "supplyCurrent")["value"], \
                f'{read_parameter} Mismatch in Exedra - NOK'

        if read_parameter == 'RMS voltage':

            assert float(expected_response) == exedra.exedra_request_device_properties(context.DeviceID, "supplyVoltage")["value"], \
                f'{read_parameter} Mismatch in Exedra - NOK'

        if read_parameter == 'Cumulative lamp burning hours':

            assert int(expected_response) == exedra.exedra_request_device_properties(context.DeviceID, "operatingHours")["value"], \
                f'{read_parameter} Mismatch in Exedra - NOK'

        logger.info(f'Exedra has the correct {read_parameter} - OK')


@then('Exedra shall report the Alarm "{alarm}" with Severity "{severity}"')
def step_impl(context, alarm, severity):

    alarm = convert_exedra_alarm_description(alarm)

    # Get the desired Alarm from the Device Properties in Exedra
    alarm_properties = exedra.exedra_request_device_properties(context.DeviceID, alarm)

    # 0 = Inactive
    # 1 = Active
    assert alarm_properties["value"], f'Device does not have Alarm "{alarm}" active'

    # 1 == Info
    # 2 == Warning
    # 3 == Error
    if severity == "Info":
        assert alarm_properties["level"] == 1, f'Expected Severity: 1 (Info), but got {alarm_properties["level"]} instead'
    elif severity == "Warning":
        assert alarm_properties["level"] == 2, f'Expected Severity: 2 (Warning), but got {alarm_properties["level"]} instead'
    elif severity == "Error":
        assert alarm_properties["level"] == 3, f'Expected Severity: 3 (Error), but got {alarm_properties["level"]} instead'
    else:
        logger.error(f'Unknown Alarm severity: {severity} - NOK')
        return False

    logger.info(f'Exedra shows the alarm {alarm} with severity {severity} - OK')
    # exedra.exedra_request_device_properties(context.DeviceID, "lampFailure")["level"]


@then('Exedra shall report the Alarm "{alarm}" as inactive')
def step_impl(context, alarm):

    alarm = convert_exedra_alarm_description(alarm)

    # Get the desired Alarm from the Device Properties in Exedra
    alarm_properties = exedra.exedra_request_device_properties(context.DeviceID, alarm)

    # 0 = Inactive
    # 1 = Active
    assert alarm_properties["value"] == 0, f'Device has Alarm "{alarm}" active'

    logger.info(f'Exedra shows the alarm {alarm} as inactive - OK')


def convert_exedra_alarm_description(alarm):

    if alarm == "Lamp Failure":  # ERROR
        alarm = "lampFailure"

    elif alarm == "Lamp power is greater than expected":  # WARNING
        alarm = "lampPowerTooHigh"

    elif alarm == "Lamp power is smaller than expected":  # WARNING
        alarm = "lampPowerTooLow"

    elif alarm == "Power factor is below the threshold":  # WARNING
        alarm = "powerFactorTooLow"

    elif alarm == "Supply voltage is above the threshold":  # ERROR
        alarm = "supplyVoltageTooHigh"

    elif alarm == "Supply voltage is below the threshold":  # ERROR
        alarm = "supplyVoltageTooLow"

    elif alarm == "Controller Failure":  # ERROR
        alarm = "controllerFailure"

    elif alarm == "DALI bus short circuit":  # ERROR
        alarm = "daliBusShort"

    elif alarm == "DALI communication failure":  # WARNING
        alarm = "daliCommFailure"

    elif alarm == "Luminaire asset information missing":  # INFO
        alarm = "luminaireAssetInformationMissing"

    elif alarm == "Invalid RFID read":  # INFO
        alarm = "luminaireFaultyRFID"

    elif alarm == "Temperature is above the threshold":  # WARNING
        alarm = "highTemperature"

    else:
        assert False, f'Unknown Alarm {alarm} to read from Exedra - NOK'

    return alarm

