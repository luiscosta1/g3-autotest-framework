from behave import given, when, then
import config
import imperium
import exedra
from time import sleep
from logs import logger
import coap
from g3_c2d_messages import build_g3_c2d_message


#################################
#   Given Steps Implementation  #
#################################

@given('the "{read_parameter}" exists in "{platform}"')
def step_impl(context, read_parameter, platform):

    if platform == 'Imperium':
        # Verify whatever in Imperium Here

        if read_parameter == 'Asset ID':
            # Check if Asset ID exists in Imperium
            if imperium.imperium_request_asset_id(context.asset_id): # Request to Imperium HERE
                logger.info(f"{read_parameter} exists in {platform} - OK")

                # Check if Asset ID has a Dimming Curve
                dimming_curve = imperium.imperium_request_dimming_curve(context.asset_id)
                if dimming_curve is not None:
                    # Asset has a dimming curve
                    return True
                else:
                    assert False, f"Asset {read_parameter} has no Dimming Curve! - NOK"

            else:
                assert False, f"{read_parameter} exists in {platform} - NOK"

        elif read_parameter == 'Device':
            if context.DeviceID: # Request to Imperium HERE
                logger.info(f"{read_parameter} exists in {platform} - OK")
                return True
            else:
                assert False, f"{read_parameter} exists in {platform} - NOK"

        else:
            assert False, f'Unknown Parameter {read_parameter} to read from {platform} - NOK'

    elif platform == "Exedra":
        # Verify whatever in Exedra Here

        if read_parameter == 'Device':

            if exedra.exedra_request_device_id(context.DeviceID):
                logger.info(f"{read_parameter} exists in {platform} - OK")
                return True
            else:
                assert False, f"{read_parameter} exists in {platform} - NOK"

        elif read_parameter == 'ADD read_parameter HERE':
            return True

        else:
            assert False, f"Unknown Parameter {read_parameter} to read from {platform} - NOK"

    else:
        assert False, f"Unknown {platform} - NOK"

################################
#   When Steps Implementation  #
################################


@when('a "{action}" is triggered via Imperium')
def step_impl(context, action):

    if action == "Firmware Downgrade":

        # isolate the release version and convert it to integer: e.g: "3.32.16.12" -> 12
        release_version = int(context.firmware_initial[context.firmware_initial.rfind(".")+1:])
        # decrement the release version
        release_version -= 1

        # create the final firmware variable and save it to context
        context.firmware_final = context.firmware_initial[:context.firmware_initial.rfind(".")+1] \
                                 + str(release_version)

        # Check if firmware below exists in FW Database
        if imperium.imperium_request_firmware_properties(context.firmware_final) is None:
            assert False, f'Firmware "{context.firmware_final}" not found in Firmware Management API'

        # Trigger Firmware Update:
        if imperium.imperium_trigger_firmware_update(device_id=context.DeviceID, target_firmware=context.firmware_final, is_debug=False):
            logger.info(f"Starting Firmware Downgrade to {context.firmware_final} - OK")
            return True
        else:
            assert False, f'Firmware update to {context.firmware_final} was not possible'

    elif action == "Firmware Update":
        # isolate the release version and convert it to integer: e.g: "3.32.16.12" -> 12
        release_version = int(context.firmware_initial[context.firmware_initial.rfind(".") + 1:])
        # increment the release version
        release_version += 1

        # create the final firmware variable and save it to context
        context.firmware_final = context.firmware_initial[:context.firmware_initial.rfind(".") + 1] \
                                 + str(release_version)

        # Check if firmware above exists in FW Database
        if imperium.imperium_request_firmware_properties(context.firmware_final) is None:
            assert False, f'Firmware "{context.firmware_final}" not found in Firmware Management API'

        # Trigger Firmware Update:
        if imperium.imperium_trigger_firmware_update(device_id=context.DeviceID, target_firmware=context.firmware_final, is_debug=False):
            logger.info(f"Starting Firmware Update to {context.firmware_final} - OK")
            return True
        else:
            assert False, f'Firmware update to {context.firmware_final} was not possible'

    elif action == "Illegal Firmware Update":
        # Firmware update to an illegal package shall fail. Therefore:
        context.firmware_final = context.firmware_initial

        # Check if illegal firmware exists in FW Database
        if imperium.imperium_request_firmware_properties(config.fw_illegal) is None:
            assert False, f'Firmware "{config.fw_illegal}" not found in Firmware Management API'

        # Trigger Firmware Update: FAKE versions have debug URL!
        if imperium.imperium_trigger_firmware_update(device_id=context.DeviceID, target_firmware=config.fw_illegal, is_debug=True):
            logger.info(f"Starting Firmware Update to {config.fw_illegal} (Illegal Firmware)- OK")
            return True
        else:
            assert False, f'Firmware update to {config.fw_illegal} was not possible'

    else:
        assert False, f'Unknown action "{action}" to perform from Imperium - NOK'


@when('the device is written the "{write_parameter}" to "{value}" via Imperium')
@given('the device is written the "{write_parameter}" to "{value}" via Imperium')
def step_impl(context, write_parameter, value):

    coap_parameter = ""
    if write_parameter == "High Active power threshold":
        # need to change dimming curve parameters before sending it:
        if value == "higher value":
            # # Set the Low at High and High at High Thresholds to higher values, to Clear the Power High Alarm
            config.dimcurve_dict["LowWattMinDim"] = "0"
            config.dimcurve_dict["HighWattMinDim"] = "200"
            config.dimcurve_dict["LowWattMaxDim"] = "1"
            config.dimcurve_dict["HighWattMaxDim"] = "201"

        elif value == "lower value":
            # # Set the Low at High and High at High Thresholds to Lower values, to Trigger the Power High Alarm
            config.dimcurve_dict["LowWattMinDim"] = "0"
            config.dimcurve_dict["HighWattMinDim"] = "0.5"
            config.dimcurve_dict["LowWattMaxDim"] = "0.6"
            config.dimcurve_dict["HighWattMaxDim"] = "0.7"
        else:
            assert False, f'Unknown value  "{value}" to write to "{write_parameter}" - NOK'

        # Send Dimming Curve:
        if context.luminaire_type == "1-10V":
            coap_parameter = "1_10V_dim_curve"
        else:
            coap_parameter = "DALI_dim_curve"

    elif write_parameter == "Low Active power threshold":
        # need to change dimming curve parameters before sending it:
        if value == "higher value":
            # Set the Low at Low and High at Low Thresholds to higher values, to Trigger the Power Low Alarm
            config.dimcurve_dict["LowWattMinDim"] = "100"
            config.dimcurve_dict["HighWattMinDim"] = "200"
            config.dimcurve_dict["LowWattMaxDim"] = "101"
            config.dimcurve_dict["HighWattMaxDim"] = "201"

        elif value == "lower value":
            # Set the Low at Low and High at Low Thresholds to Lower values, to Clear the Power Low Alarm
            config.dimcurve_dict["LowWattMinDim"] = "0"
            config.dimcurve_dict["HighWattMinDim"] = "200"
            config.dimcurve_dict["LowWattMaxDim"] = "1"
            config.dimcurve_dict["HighWattMaxDim"] = "201"
        else:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'

        # Send Dimming Curve:
        if context.luminaire_type == "1-10V":
            coap_parameter = "1_10V_dim_curve"
        else:
            coap_parameter = "DALI_dim_curve"

    elif write_parameter == "Low Power Factor threshold":
        # need to change dimming curve parameters before sending it:
        if value == "higher value":
            config.PowerFactorLimit = "0.99"
        elif value == "lower value":
            config.PowerFactorLimit = "0.01"
        elif 0 < int(value) <= 1:  # user provides a specific number
            config.PowerFactorLimit = value
        else:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'

        # Send the Low Power Factor Threshold
        coap_parameter = "power_factor_limit"

    elif write_parameter == "Temperature Hysteresis Time":
        try:
            assert 0 < int(value) < 100, f'Invalid value "{value}" for "{write_parameter}"'
        except ValueError:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'

        config.temp_time_hyst = value
        # Send the Temperature Hysteresis Time
        coap_parameter = "high_temp_hyst"

    elif write_parameter == "Voltage Hysteresis Time":
        try:
            assert 0 < int(value) < 100, f'Invalid value "{value}" for "{write_parameter}"'
        except ValueError:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'

        config.VoltageTimeHyst = value
        # Send the Voltage Hysteresis Time
        coap_parameter = "voltage_hyst"

    elif write_parameter == "High Temperature Threshold":
        if value == "higher value":
            config.temp_max = "100"
        elif value == "lower value":
            config.temp_max = "10"
        elif 0 < int(value) < 100:  # user provides a specific number
            config.temp_max = value
        else:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'

        # Send the High Temperature Threshold
        coap_parameter = "high_temp_limit"

    elif write_parameter == "High Voltage Threshold":
        if value == "higher value":
            config.SPHighVoltage = "300"
        elif value == "lower value":
            config.SPHighVoltage = "100"
        elif 0 < int(value) < 500:  # user provides a specific number
            config.SPHighVoltage = value
        else:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'

        # Send the High Voltage Threshold
        coap_parameter = "high_voltage_limit"

    elif write_parameter == "Low Voltage Threshold":
        if value == "higher value":
            config.SPLowVoltage = "300"
        elif value == "lower value":
            config.SPLowVoltage = "100"
        elif 0 < int(value) < 500:  # user provides a specific number
            config.SPLowVoltage = value
        else:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'

        # Send the Low Voltage Threshold
        coap_parameter = "low_voltage_limit"

    elif write_parameter == "Update Server APNs":
        if value == "Test Environment":
            config.exedra_Rurl = config.EXEDRA_RURL_TST
        elif value == "Development Environment":
            config.exedra_Rurl = config.EXEDRA_RURL_DEV
        elif value == "Production Environment":
            config.exedra_Rurl = config.EXEDRA_RURL_PRD
        else:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'
        # Send the Update Server APNs
        coap_parameter = "exedra_upd_server"

    elif write_parameter == "Update Server Notification APNs":
        if value == "Test Environment":
            config.exedra_Rurl = config.EXEDRA_RURL_TST
        elif value == "Development Environment":
            config.exedra_Rurl = config.EXEDRA_RURL_DEV
        elif value == "Production Environment":
            config.exedra_Rurl = config.EXEDRA_RURL_PRD
        else:
            assert False, f'Unknown value "{value}" to write to "{write_parameter}" - NOK'

        # Send the Update Server Notification APNs
        coap_parameter = "exedra_upd_notif_server"

    else:
        assert False, f'Unknown Parameter "{write_parameter}" to write on Device - NOK'

    # Send COAP message via Imperium:
    # build the payload
    coap_payload = coap.create_coap_payload(coap_parameter, context.DeviceID)
    # encapsulate the G3 message body inside the Imperium request payload
    message_body = build_g3_c2d_message(coap_payload)
    # send the request
    response = imperium.imperium_send_coap_to_device(message_body, context.DeviceID)

    if response.status_code != 202:
        assert False, f'Failed to write "{write_parameter}" to {value} on Device via Imperium - NOK'

    logger.info(f'Write "{write_parameter}" to "{value}" on Device via Imperium - OK')
    return True


################################
#   Then Steps Implementation  #
################################


@then("Imperium shall report the device as Present")
def step_impl(context):

    # save the Imperium device properties in context
    context.device_properties = imperium.imperium_request_device_id(context.DeviceID)

    if context.device_properties:
        logger.info(f'Device {context.DeviceID} in Imperium - OK')
    else:
        assert False, f'Device {context.DeviceID} not in Imperium - NOK'


@then('Imperium shall report the correct "{read_parameter}"')
def step_impl(context, read_parameter):

    # save the Imperium device properties in context
    context.device_properties = imperium.imperium_request_device_id(context.DeviceID)

    # Only move forward in the device properties exist in context
    if context.device_properties:

        if read_parameter == 'GPS Coordinates':
            if context.device_properties["latitude"] == float(config.gps_lat) \
                    and context.device_properties["longitude"] == float(config.gps_long):
                logger.info(f'Imperium has the correct {read_parameter} - OK')
            else:
                logger.error(f'{context.device_properties["latitude"]=} vs {config.gps_lat=}')
                logger.error(f'{context.device_properties["longitude"]=} vs {config.gps_long=}')
                assert False, f'{read_parameter} Mismatch in Imperium - NOK'

            # logger.info(f'Imperium has the correct {read_parameter} - OK - !!!!!!!!STUB!!!!!!!!')

        elif read_parameter == 'Asset ID':
            if context.device_properties["assetId"] == context.asset_id:
                logger.info(f'Imperium has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Imperium - NOK'

        elif read_parameter == 'Tenant':
            if context.device_properties["tenant"] == config.tenant:
                logger.info(f'Imperium has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Imperium - NOK'

        elif read_parameter == 'Tenant Timezone':
            assert True
            # TODO: Do this only when we have the Tenant configurations from Imperium
            # if context.device_properties["tenant"] == config.tenant:
            #     logger.info(f'Imperium has the correct {read_parameter} - OK')
            # else:
            #     logger.error(f'{read_parameter} Mismatch in Imperium - NOK')
            #     assert False

        elif read_parameter == 'Firmware Version':
            if context.device_properties["firmware"]["installedVersion"] == context.firmware_initial:
                logger.info(f'Imperium has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Imperium - NOK'

        elif read_parameter == 'Final Firmware Version':
            if context.device_properties["firmware"]["installedVersion"] == context.firmware_final:
                logger.info(f'Imperium has the correct {read_parameter} - OK')
            else:
                assert False, f'{read_parameter} Mismatch in Imperium - NOK'
        else:
            assert False, f'Unknown Parameter {read_parameter} to read from Imperium - NOK'

    else:
        assert False, f'No properties in Imperium for {context.DeviceID}. - NOK'


@then('Imperium shall report the "{read_parameter}" as "{expected_response}"')
def step_impl(context, read_parameter, expected_response):

    # Only move forward in the device properties exist in context
    if context.device_properties:

        if read_parameter == 'Asset Status':
            if context.device_properties["assetStatus"] == expected_response:
                logger.info(f'{read_parameter} in Imperium is {expected_response} - OK')
            else:
                assert False, f'{read_parameter} in Imperium is {context.device_properties["assetStatus"]}, not {expected_response} - NOK'

        elif read_parameter == 'Driver Status':
            if context.device_properties["driverStatus"] == expected_response:
                logger.info(f'{read_parameter} in Imperium is {expected_response} - OK')
            else:
                assert False, f'{read_parameter} in Imperium is {context.device_properties["assetStatus"]}, not {expected_response} - NOK'

        else:
            assert False, f'Unknown Parameter {read_parameter} to read from Imperium - NOK'

    else:
        logger.error(f'No properties of {context.DeviceID}. - NOK')
        logger.error('Call "Then Imperium shall report the device as Present" first.')
        assert False, f'No properties of {context.DeviceID}. - NOK'
