import json
import requests
import io
import glob
import os
import sys

from zipfile import ZipFile
from requests.auth import AuthBase, HTTPBasicAuth

baseAddr_XRAY='https://xray.cloud.getxray.app/'
baseAddr_jira = 'https://schreder-hyperion.atlassian.net/rest/api/2/issue/'

headers_XRAY = {
   "Accept": "application/json",
   "Content-Type": "application/json",
  }

headers_jira = {
    "Accept": "application/json",
    "X-Atlassian-Token": "no-check"
 }


class XrayAPI(AuthBase):

   def __init__(self,clientId,clientSecret,usr_jira,jira_token):
      '''
      Initialize the Authentication for the API XRAY Jira platform 
      '''

      self._exec_keys = ''
      auth_data_XRAY = {
         'client_id': str(clientId),
         'client_secret': str(clientSecret)
      }
      try:
         response = requests.post(
                                 baseAddr_XRAY + 'api/v2/authenticate',
                                 data = json.dumps(auth_data_XRAY),
                                 headers = headers_XRAY
                                 )
         self._auth_Jira = HTTPBasicAuth(usr_jira, jira_token) # TODO Encrypt tokens...
      except:
         print("Connection Error: Cannot Authenticate with Xray Server")
         sys.exit()
      else:
         self.auth_token = json.loads(response.text)
         headers_XRAY.update(dict({"Authorization" : 'Bearer '+self.auth_token}))  # Update the Authorization entry with the Obtained Token.

   def export_tests_cucumber(self,key):
      """
      Export the Cucumber file from the XRAY API using th 'Key' parameter as the issue key (Normally the reference to the Test Execution).
      The API returns zip files. The files are then unzipped in the './features' folder
      """
      response = requests.get(
                              baseAddr_XRAY + 'api/v2/export/cucumber?keys='+str(key),
                              headers = headers_XRAY
                             )
      if(response.status_code == 200):# Good request
      # Clean/Deletes all the files in the 'features' folder before unziping the new files.'
         self._exec_keys += key
         files = glob.glob('./anx/*')
         for f in files:
            os.remove(f)
         files = glob.glob('./features/*')
         for f in files:
            os.remove(f)
         # Read the zip file in the byte stream received from the XRAY API and then unzip it
         zip_features = ZipFile(io.BytesIO(response.content), "r")
         zip_features.extractall('./features')
      else:
         print("Bad Request: Cannot Retrieve Key " + str(key) + " ressources.")
         sys.exit()

   def import_tests_cucumber(self):
      '''
      Import the cucumber.json file to the XRAY platform using API. The file is in the "results" folder
      '''
      with open('results/cucumber.json') as json_file:
         data = json.load(json_file)

      response = requests.post(
                              baseAddr_XRAY + 'api/v2/import/execution/cucumber',
                              json = data,
                              headers = headers_XRAY
                              )
      print (response.content)

   def attach_test_logs(self, log_filename=""):
      '''
      Update the Test Key with the test.log generated during testing
      '''
      requests.post(
                              baseAddr_jira + self._exec_keys + '/attachments',
                              headers = headers_jira,
                              auth = self._auth_Jira,
                              files = {
                                       "file": (log_filename, open(log_filename, 'rb'), "application-type")
                                      }
                              )

   def download_attachment(self):
      '''
      Download the attachments from the tests exported with XRAY.
      '''
      tests_list = []
      files = glob.glob('./features/*')
      for f in files:
         test_file = open(str(f),'r',encoding="utf8")
         srch_str = test_file.read()
         srch_indx = 0
         while True:
            xcv = srch_str.find('@TEST_NGF-',srch_indx)
            if(xcv == -1):
               break
            else:
               strt_idx = xcv + len('@TEST_')
               end_idx = srch_str.find(' ',xcv)
               if(end_idx-strt_idx > 10):
                  end_idx = srch_str.find('\n',xcv)
               tests_list.append(srch_str[strt_idx:end_idx])
               srch_indx = xcv + len('@TEST_NGF-')
      for test in tests_list:
         req = requests.get(
                              baseAddr_jira + test,
                              headers = headers_jira,
                              auth = self._auth_Jira,
                           )
         data = req.json()
         for attach in data['fields']['attachment'] :
            rs = requests.get(
                                 attach['content'],
                                 headers = headers_jira,
                                 auth = self._auth_Jira
                              )
            with open("./anx/"+attach['filename'], "wb") as f:
               f.write(rs.content.decode('iso-8859-1').encode('utf8'))