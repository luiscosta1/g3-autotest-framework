####################################################################################
# Filename:     run_tests.py
# Author:       Thimotée Gerrits
# Modified by:  Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Version:      2.0
#
# Filetype:     Main file
# Description:  Main file to run with the desired Test Executions
# Status:       Under development
# TODO:         - call behave_main(my_path) instead of os.system('cmd /c "behave"')
####################################################################################
""" This is the Main file to run with the desired Test Executions """

import sys
import argparse
import os
import json
from xrayAPI.xrayapi import XrayAPI
import logging
from logs import logger
from behave.__main__ import main as behave_main
from behave.model import Scenario


def run_tests(args):

    # Loads the keys & tokens for access to the Jira and Xray API.
    with open("credentials/credentials.json") as f:
        credentials = json.load(f)

    # Init Xray API, loading the User and tokens from the file above
    xAPI = XrayAPI(credentials["xray"]["user"],
                   credentials["xray"]["token"],
                   credentials["jira"]["user"],
                   credentials["jira"]["token"])

    if args.loglevel:
        logger.setLevel(args.loglevel)
        logger.debug(f'Log Level set to {args.loglevel}')
    else:
        # if not provided, set default log level to INFO
        logger.setLevel('INFO')

    log_filename = 'logs/logfile.log'

    # Run the Script. Will Select and execute Options from input
    if args.k:
        logger.info('***************************************')
        logger.info(f'Importing from XRAY Test Execution Id: {str(args.k)}')
        xAPI.export_tests_cucumber(args.k)
        xAPI.download_attachment()

    logger.info("*************BEHAVE*******************")

    # os.system('cmd /c "behave"')  # execute the 'behave' command in the command line
    Scenario.continue_after_failed_step = True
    behave_main('./')

    if args.e:
        logger.info('***************************************')
        logger.info('Exporting to XRAY Test Execution')
        xAPI.import_tests_cucumber()

    if args.l:
        logger.info('***************************************')
        logger.info('Exporting to Test Logs')
        xAPI.attach_test_logs(log_filename)


if __name__ == "__main__":
    #### Argument parser. run "python run_test.py --help" to get more details
    if len(sys.argv) < 2:
        print(f'No arguments provided. Please run "run_tests -h" for help')
        exit()
    else:
        parser = argparse.ArgumentParser()
        parser.add_argument("--k",
                            help="Import the tests from Xray project. Must provide a key. i.e: run_test.py --k NGF-3216")
        parser.add_argument("-e", help="Export the Cucumber test results to Xray test project", action="store_true")
        parser.add_argument("-l", help="Export the tests.log file to the Xray test execution as an annex",
                            action="store_true")
        parser.add_argument("--loglevel",
                            help="Set the details to the desired loglevel. DEBUG, INFO (default), WARNING, ERROR")
        user_input_args = parser.parse_args()

        print(f'{user_input_args=}')
        run_tests(user_input_args)
