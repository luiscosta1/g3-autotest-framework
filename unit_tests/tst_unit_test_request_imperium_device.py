import unittest
import imperium
import config


class MyTestCase(unittest.TestCase):
    def test_imperium_request_device_id(self):
        self.assertEqual(type(imperium.imperium_request_device_id(device_id=config.DeviceID)), dict)  # add assertion here


if __name__ == '__main__':
    unittest.main()
