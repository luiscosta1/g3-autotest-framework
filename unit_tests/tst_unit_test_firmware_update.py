import unittest
import imperium
import config


class MyTestCase(unittest.TestCase):
    def test_imperium_trigger_firmware_update(self):
        self.assertEqual(imperium.imperium_trigger_firmware_update(device_id="0013A20041C54F33", target_firmware=config.fw_update), True)  # add assertion here


if __name__ == '__main__':
    unittest.main()
