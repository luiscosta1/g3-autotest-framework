import unittest
import imperium
import config


class MyTestCase(unittest.TestCase):
    def test_imperium_request_asset_id(self):
        self.assertEqual(imperium.imperium_request_asset_id(asset_id=config.AssetID), True)  # add assertion here


if __name__ == '__main__':
    unittest.main()
