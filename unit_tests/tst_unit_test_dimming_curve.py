import unittest
import imperium
import config


class MyTestCase(unittest.TestCase):
    def test_imperium_request_dimming_curve(self):
        self.assertEqual(type(imperium.imperium_request_dimming_curve(asset_id=config.AssetID)), list)  # add assertion here


if __name__ == '__main__':
    unittest.main()
