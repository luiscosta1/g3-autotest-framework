import unittest
import imperium
import config


class MyTestCase(unittest.TestCase):
    def test_imperium_request_firmware_properties(self):
        self.assertEqual(type(imperium.imperium_request_firmware_properties(config.fw_current)), list)  # add assertion here


if __name__ == '__main__':
    unittest.main()
