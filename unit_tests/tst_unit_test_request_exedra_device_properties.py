import unittest
import exedra
import config


class MyTestCase(unittest.TestCase):

    def test_exedra_request_device_id(self):
        self.assertEqual(exedra.exedra_request_device_id(config.DeviceID), True)  # add assertion here

    def test_exedra_request_device_LightLevel(self):
        self.assertEqual(exedra.exedra_request_device_properties(config.DeviceID, "actualLightState")["value"], 44)  # add assertion here

    def test_exedra_request_device_FirmwareVersion(self):
        self.assertEqual(exedra.exedra_request_device_properties(config.DeviceID, "firmwareVersion"), config.fw_current)  # add assertion here

    def test_exedra_request_device_lampFailure_value(self):
        self.assertEqual(exedra.exedra_request_device_properties(config.DeviceID, "lampFailure")["value"], 0)  # add assertion here

    def test_exedra_request_device_lampFailure_level(self):
        self.assertEqual(exedra.exedra_request_device_properties(config.DeviceID, "lampFailure")["level"], 0)  # add assertion here


if __name__ == '__main__':
    unittest.main()
