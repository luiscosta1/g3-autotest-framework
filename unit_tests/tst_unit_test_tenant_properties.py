import unittest
import imperium
import config


class MyTestCase(unittest.TestCase):
    def test_imperium_request_tenant_id(self):
        self.assertEqual(type(imperium.imperium_request_tenant_properties(config.tenant)), dict)  # add assertion here


if __name__ == '__main__':
    unittest.main()
