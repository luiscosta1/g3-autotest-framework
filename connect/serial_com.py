import serial
from .driver import Driver

class SerialCom(Driver):

    def __init__(self,baud='9600',com_port='COM1'):
        """
        Initialize the class -> Will not open the port. Use open_com func for that
        """      
        self._ser = serial.Serial(port=None,baudrate=baud,timeout=5)
        self._ser.port = com_port
        self._readBuf = ""
        self._prt = com_port
        self._lastLength = 0
        super().__init__()

    def __exit__(self):
        self.close()    

    def open(self) -> bool:
        """
        Open the port set by 'self._ser.port'. Return True if success in opening.
        """        
        if(self._ser.is_open is False):
            try:  
                self._ser.open()
                self._ser.set_buffer_size(rx_size=32768)
                self._log_th.start()
                return True
            except serial.SerialException as e:
                return False               

    def close(self):
        """
        Close the port
        """         
        if(self._ser.is_open is True):
            self._ser.close()

    def getc(self,size, timeout=1):
        '''
        Read and return the number of bytes as defined in the "size" parameter. Used by the XModem lib
        '''        
        if(self._ser.is_open is True):
            return self._ser.read(size)

    def putc(self,byt_data):
        '''
        Write bytes to the serial line. Used by the XModem lib
        '''
        if(self._ser.is_open is True):
            return self._ser.write(byt_data)                    

    def write_command(self,command):
        """
        Write/Send a command to the serial line.

        """        
        if(self._ser.is_open is True):
            enc_com = bytes(command + "\r",'utf-8') # Command must be encoded in bytes before sending through serial line
            self._ser.write(enc_com)

    def read_command_response(self):
        """
        Read the response of a console command. The response will end with a '>' character
        """
        enc_resp=self._ser.read_until(expected=b'>')
        self._lastLength = len(self._readBuf) # Keep the length of the read Buffer. No need to search for the response in previous index
        self._readBuf += str(enc_resp,encoding = "ascii",errors ="ignore") # Command muste be decode from byte to string
        self._readBuf = self._readBuf.replace("\x00",'')# remove all the '\x00' entries in the response.

    def get_command_response(self,command):
        """
        Send/Write a command on the Serial line and return the response
        """        
        with self._mutex:
            # Mutex Set -> Do not interfere with Logging and vice-versa.
            self.write_command(command)
            self.read_command_response()
            #Log the send/receive command
            str_clr = self._readBuf[self._lastLength:].replace('\n','')
            with open('logs/tests.log', 'a') as f:
                f.write(str_clr)  
            self.clean_read_buffer()                          
        return str_clr # Will only send the str with the response. 

    def read_all_bytes_availables(self):
        '''
        Will read all the available bytes in the IO Buffer from the OS
        '''
        enc_resp = self._ser.read_all()
        self._readBuf += str(enc_resp,encoding = "ascii",errors ="ignore") # Command must be decode from byte to string
        self._readBuf = self._readBuf.replace("\x00",'') # remove all the '\x00' entries in the response.

    def clean_read_buffer(self):
        """
        Clean/Reset the read buffer
        """
        self._readBuf = ""

    def get_read_buffer(self):
        """
        returns the read buffer
        """
        return self._readBuf

