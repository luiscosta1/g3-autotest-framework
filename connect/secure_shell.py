import time

from paramiko import client,Channel
from paramiko.ssh_exception import AuthenticationException
from .driver import Driver


class SecureShell(Driver):

    def __init__(self, target_ip='192.168.1.95', username='pi', password='', port=22, auto_add_keys=True):
        """
        Initialize the class -> Will not open the SSH Connection. Use open() func for that
        """      
        self._target_ip = target_ip
        self._username = username
        self._password = password
        self._port = port
        self._shell = None
        self._client = client.SSHClient()        
        if auto_add_keys:
            self._client.set_missing_host_key_policy(client.AutoAddPolicy())
        super().__init__()

    def __exit__(self):
        self.close()

    def open(self):
        """
        Open the SSH connection. Return True if success in opening.
        """          
        try:
            if((self._shell is None) or (self._shell.active is False)):
                self._client.connect(
                                        self._target_ip,
                                        port=self._port,
                                        username=self._username,
                                        password=self._password,
                                        look_for_keys=False
                                    )
                self._shell = self._client.invoke_shell(term='xterm',width=512)
            # Starts the minicom session.
                self._shell.send('minicom --color=on -w -D /dev/ttyS0\r')
                time.sleep(1)
                self._log_th.start()
            return True                    
        except (AuthenticationException,client.SSHException):
            return False

    def close(self):
        """
        Close the SSH port
        """   
        if(Channel.active):      
            self._client.close()        

    def getc(self,size, timeout=1):
        '''
        Read and return the number of bytes as defined in the "size" parameter. Used by the XModem lib
        '''        
        return self._shell.recv(size)

    def putc(self,byt_data):
        '''
        Write bytes to the SSH line. Used by the XModem lib
        '''
        return self._shell.send(byt_data)   

    def write_command(self,command):
        """
        Write/Send a command to the SSH line.

        """        
        self._shell.send(command+'\r')   

    def read_command_response(self):
        """
        Read the response of a console command. The response will end with a '>' character
        """
        resp = self._shell.recv(2048)
        timeout = 5 # Setting timout as 5 seconds
        timer = 0
        while(timer<timeout and (resp.find(b'>') == -1)):
            resp += self._shell.recv(2048)
            timer += 1
            time.sleep(1)
        self._lastLength = len(self._readBuf) # Keep the length of the read Buffer. No need to search for the response in previous index            
        self._readBuf += str(resp,encoding='ascii',errors='ignore').replace("\x00",'')# remove all the '\x00' entries in the response.

    def get_command_response(self,command):
        """
        Send/Write a command on the SSH line and return the response
        """        
        with self._mutex:
            # Mutex Set -> Do not interfere with Logging and vice-versa.
            self.write_command(command)
            self.read_command_response()
            #Log the send/receive command
            str_clr = self._readBuf[self._lastLength:].replace('\n','')
            with open('logs/tests.log', 'a') as f:
                f.write(str_clr)  
            self.clean_read_buffer()                          
        return str_clr # Will only send the str with the response. 

    def read_all_bytes_availables(self):
        '''
        Will read all the available bytes in the IO Buffer from the OS
        '''
        self._readBuf += str(self._shell.recv(2048),encoding='ascii',errors='ignore')
         
    def clean_read_buffer(self):
        """
        Clean/Reset the read buffer
        """
        self._readBuf = ""

    def get_read_buffer(self):
        """
        returns the read buffer
        """
        return self._readBuf