import time
import glob
import os
from threading import Thread,Lock


class Driver:
    def __init__(self):
        self._readBuf = ""        
        files = glob.glob('./logs/*')
        for f in files:
            os.remove(f)             
        # Start the Logging Thread
        self._log_th = Thread(target=Driver.log_in_serial_to_file, args=(self,),daemon = True) 
        self._mutex = Lock() # Create a mutex

    def open(self):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()        

    def getc(self, size, timeout):     
        raise NotImplementedError()   

    def putc(self,byt_data):        
        raise NotImplementedError()

    def read_command_response(self):
        raise NotImplementedError()

    def write_command(self, command):
        raise NotImplementedError()

    def get_command_response(self):
        raise NotImplementedError()        

    def read_all_bytes_availables(self):
        raise NotImplementedError()

    def clean_read_buffer(self):
        raise NotImplementedError()

    def get_read_buffer(self):
        raise NotImplementedError()

    def log_in_serial_to_file(s):
##Thread to Log the commands read/write from the device. Logs are kept in the ./logs/tests.logs file.
        while True:
            with s._mutex:
            # Mutex Set -> Do not interfere with writing/reading a command and vice-versa.
                s.read_all_bytes_availables()
                str_clr = s._readBuf.replace('\n','')
                with open('logs/tests.log', 'a') as f:
                    f.write(str_clr)
                s.clean_read_buffer()
            time.sleep(0.5)        