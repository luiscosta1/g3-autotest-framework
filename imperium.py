####################################################################################
# Filename:     imperium.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Version:      1.0
#
# Filetype:     Library file
# Description:  Functions to support HTTP requests to Device/Asset/Firmware/Tenant Management APIs  (Imperium)
# STATUS:       New
# Limitation:
# TODO:         make imperium as a Class (?)
####################################################################################

"""Functions to support HTTP requests to Device/Asset/Firmware/Tenant Management APIs  (Imperium)"""


# import requests - Changed this. verify if the below import works
from requests import request
from time import time
import json
import config
from logs import logger


def azure_request_token(api):
    """
    This function requests the Imperium Bearer token to the desired API
    The request is only performed if the current token validity has expired
    Returns the response from the server
    """

    logger.debug(f'Requesting bearer from {api}')

    if api == 'asset_management_api':
        token_validity = config.asset_management_bearer_token_validity
        payload = config.azure_get_am_api_credentials
    elif api == 'device_management_api':
        token_validity = config.device_management_bearer_token_validity
        payload = config.azure_get_dm_api_credentials
    elif api == 'tenant_management_api':
        token_validity = config.tenant_management_bearer_token_validity
        payload = config.azure_get_tm_api_credentials
    elif api == 'firmware_management_api':
        token_validity = config.firmware_management_bearer_token_validity
        payload = config.azure_get_fw_api_credentials
    else:
        logger.error(f'Unknown API to request Token: {api}')
        return False

    # Only request new Token if Current Time is greater than the token validity
    if time() > token_validity:

        headers = {"Content-Type": "application/x-www-form-urlencoded"}

        query = {'query': 'query'}

        url = "https://login.microsoftonline.com/470f9841-4d51-4cf1-adbe-5077fa8c7eda/oauth2/v2.0/token"

        # payload = config.azure_get_dm_api_credentials

        response = request(
            method="POST",
            url=url,
            headers=headers,
            params=query,
            data=payload
        )

        if response.status_code != 200:
            logger.warning(f'Azure Token Request returned: {response.status_code}')
            return False

        # Azure returns the content in bytes, we need to convert to dict
        response = json.loads(response.content)

        if api == 'asset_management_api':
            # And the Bearer Token is found in "access_token":
            config.asset_management_bearer_token = response["access_token"]
            # set the token validity to 55 min from now
            config.asset_management_bearer_token_validity = time() + 3300

        elif api == 'device_management_api':
            # And the Bearer Token is found in "access_token":
            config.device_management_bearer_token = response["access_token"]
            # set the token validity to 55 min from now
            config.device_management_bearer_token_validity = time() + 3300

        elif api == 'tenant_management_api':
            # And the Bearer Token is found in "access_token":
            config.tenant_management_bearer_token = response["access_token"]
            # set the token validity to 55 min from now
            config.tenant_management_bearer_token_validity = time() + 3300

        elif api == 'firmware_management_api':
            # And the Bearer Token is found in "access_token":
            config.firmware_management_bearer_token = response["access_token"]
            # set the token validity to 55 min from now
            config.firmware_management_bearer_token_validity = time() + 3300

        else:
            return False

    return True


def imperium_request(url, api, request_type, payload=''):
    """
    This is the most important request function and the base for all requests to Imperium
    Performs requests to the desired API
    Returns the response from the server
    """

    # First, request the Imperium Access Token to Azure. Exit if it's unsuccessful
    success = azure_request_token(api=api)
    if not success:
        return ''

    logger.debug(f'Performing request to {api}')

    if api == 'asset_management_api':
        headers = {
            "Accept": "application / json, text / plain, * / *",
            "Authorization": "Bearer " + config.asset_management_bearer_token,
            "Content-Type":  "application/json",
            "exedra-subscription-key" : "3bdc7d2fc8024b87b6f58141ebd60004"
        }

    elif api == 'device_management_api':
        headers = {
            "Accept": "application / json, text / plain, * / *",
            "Authorization": "Bearer " + config.device_management_bearer_token,
            "Content-Type":  "application/json",
            "exedra-subscription-key" : "3bdc7d2fc8024b87b6f58141ebd60004"
        }

    elif api == 'tenant_management_api':
        headers = {
            "Accept": "application / json, text / plain, * / *",
            "Authorization": "Bearer " + config.tenant_management_bearer_token,
            "Content-Type":  "application/json",
            "exedra-subscription-key" : "3bdc7d2fc8024b87b6f58141ebd60004"
        }

    elif api == 'firmware_management_api':
        headers = {
            "Accept": "application / json, text / plain, * / *",
            "Authorization": "Bearer " + config.firmware_management_bearer_token,
            "Content-Type":  "application/json",
            "exedra-subscription-key" : "3bdc7d2fc8024b87b6f58141ebd60004"
        }
    else:
        logger.error(f'Unknown API to make request to: {api}')
        return False

    query = {
        'query': 'query'
    }

    response = request(
        request_type,
        url,
        headers=headers,
        params=query,
        data=payload
    )

    return response


###########################################################
#########  Device Management-Related functions ############
###########################################################


def imperium_send_coap_to_device(payload, device_id=config.DeviceID):  # payload must come from above
    """
    Posts a Coap Message to a specific device
    By default, device_id is the DeviceID under test
    Returns the response from the request
    """

    url = 'https://' + config.device_management_url + '/api/v1/devices/G3/' + device_id + '/command/custom'

    response = imperium_request(url=url, api='device_management_api', request_type='POST', payload=payload)

    # response shall contain the status code (202 - OK)
    return response


def imperium_request_device_id(device_id=config.DeviceID):
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the Device properties, if found
    """

    url = 'https://' + config.device_management_url + '/api/v1/device-hubs/' + config.iot_hub + \
          '/devices?searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    device = imperium_request(url=url, api='device_management_api', request_type='GET')

    # check if device ID is present in response
    if device_id not in device.text:
        logger.warning(f'Device {device_id} not found in Device Management API')
        return None

    # Device Found, return it as a Dictionary
    device = json.loads(device.text)

    return device['values'][0]


def imperium_request_device_properties(parameter, device_id=config.DeviceID) -> str:
    """
    Requests a search for a specific Device ID.
    By default, device_id is the DeviceID under test
    Returns the device Firmware version on IoT HUB
    """

    url = 'https://' + config.device_management_url + '/api/v1/device-hubs/' + config.iot_hub + \
          '/devices?searchText='

    # Append the desired device_id to the end of the URL requested
    url += device_id

    response = imperium_request(url=url, api='device_management_api', request_type='GET')

    # check if device ID is not present in response
    if device_id not in response.text:
        logger.warning(f'Device {device_id} not found in Device Management API')
        return ''

    # device is present in IoT HUB, but response is complex. Needs further processing:
    response = get_value_from_response(response, parameter)

    return response


def get_value_from_response(response, value) -> str:
    """
    Receives the data from a device in Json format
    Returns the desired parameter defined in "value"
    """

    # data_dict is created as a Python dictionary from the Json text
    data_dict = json.loads(response.text)

    # we care only about the "values" dictionary
    payload_dict = data_dict['values'][0]

    # and we only want to return the desired word. eg.'firmwareVersion', 'tenant', etc
    return payload_dict[value]

# INFO:
# response.text provides the following info (list of 3 dictionaries):
# {"count":1,
#  "nextPageToken":null,
#  "values":
#       [{"id":"0013A20041BC46C2","tenant":"Manual Registration",
#       "firmwareVersion":"3.32.15.100","type":"G3","dimmingType":"1-10V","latitude":38.67824,"longitude":-9.326725,
#       "firmwareUpdateStatus":null,"firmwareUpdateOperationId":null,"assetId":"E00401009F5A3E6E","status":"Unreachable",
#       "lastConnectedDateTime":"2021-02-24T17:42:50.424+00:00",
#       "lastCommandAcknowledgedDateTime":"2021-02-24T17:42:50.424+00:00","assetStatus":"Valid"}]}


###########################################################
#########  Asset Management-Related functions #############
###########################################################


def imperium_request_asset_id(asset_id=config.AssetID) -> bool:
    """
    Requests a search for a specific Asset ID.
    By default, asset_id is the AssetID under test
    Returns True if the asset is found
    """

    url = 'https://' + config.asset_api_url + '/platform/management/assets/api/v1/assets/'

    # Append the desired device_id to the end of the URL requested
    url += asset_id

    response = imperium_request(url=url, api='asset_management_api', request_type='GET')
    logger.debug(f"Asset ID Response: {response.text}")

    # check if Asset was found
    if response.status_code != 200:
        logger.warning(f'Asset {asset_id} not found in Asset Management API')
        return False

    return True


def imperium_request_dimming_curve(asset_id=config.AssetID):
    """
    Requests a search for a specific Asset ID.
    By default, asset_id is the AssetID under test
    If found, the dimming curve(s) is(are) returned as a dictionary
    """

    url = 'https://' + config.asset_api_url + '/platform/management/assets/api/v1/assets/'

    # Append the desired device_id to the end of the URL requested
    url += asset_id

    response = imperium_request(url=url, api='asset_management_api', request_type='GET')
    # logger.debug(f"Asset ID Response: {response.text}")

    # check if Asset was found
    if response.status_code != 200:
        logger.warning(f'Asset {asset_id} not found in Asset Management API ')
        return None

    # Asset Found, look for the Dimming curve:
    response = json.loads(response.content)

    dimming_curves = []

    if len(response["luminaires"]) == 0:
        logger.warning(f'No Dimming Curves found for Asset {asset_id}')
        return None
    else:
        for index in range(0, len(response["luminaires"])):
            dimming_curve_id = response["luminaires"][index]["dimmingCurveId"]
            dimming_curves.append(request_dimming_curve_by_id(dimming_curve_id))

    return dimming_curves


def request_dimming_curve_by_id(dimming_curve_id=None):
    """
    Requests a search for a specific Dimming Curve ID
    If found, the dimming curve is returned as a dictionary
    """

    url = 'https://' + config.asset_api_url + '/platform/management/assets/api/v1/dimmingCurves/'

    # Append the desired device_id to the end of the URL requested
    url += str(dimming_curve_id)

    response = imperium_request(url=url, api='asset_management_api', request_type='GET')
    # logger.debug(f"Dimming Curve Response: {response.text}")

    # check if Dimming Curve was found
    if response.status_code != 200:
        logger.warning(f'Dimming Curve {dimming_curve_id} not found')
        return None

    # Dimming Curve Found, return it as a Dictionary
    dimming_curve = json.loads(response.content)

    return dimming_curve


###########################################################
#########  Tenant Management-Related functions ############
###########################################################

def imperium_request_tenant_properties(tenant=config.tenant):
    """
    Requests a search for a specific Tenant.
    By default, Tenant is the one under test
    Returns Tenant properties, if found.
        found_tenant["deviceTypes"][0] are the G3 Configurations
        found_tenant["deviceTypes"][1] are the G4 Configurations
        found_tenant["tenantId"] or tenant["name"] can also be useful
    """

    url = 'https://' + config.tenant_api_url + '/platform/management/tenants/api/v1/tenants/'

    # Append the desired device_id to the end of the URL requested
    url += tenant

    response = imperium_request(url=url, api='tenant_management_api', request_type='GET')

    # check if device ID is present in response
    if tenant not in response.text:
        logger.warning(f'Tenant {tenant} not found in Tenant Management API')
        return None

    # Tenant Found, return it as a Dictionary
    found_tenant = json.loads(response.text)

    return found_tenant


###########################################################
#########  Firmware Management-Related functions ##########
###########################################################

def imperium_request_firmware_properties(firmware=config.fw_current):
    """
    Requests a search for a specific Firmware.
    By default, the Firmware is the one under test
    Returns Firmware properties, if found.
        found_firmware[0] or found_firmware[0] may be retrieved, if the search returned more than 1 result
        found_firmware[0]['deviceType'] refers to the type of controller
        found_firmware[0]['available'] means if the firmware is decommissioned or not
        found_firmware[0]['imgFileName'] is for G4 Only
    """

    url = 'https://' + config.firmware_api_url + '/platform/management/firmware/api/v1/firmware?searchText='

    # Append the desired firmware to the end of the URL requested
    url += firmware

    response = imperium_request(url=url, api='firmware_management_api', request_type='GET')

    # check if Firmware is present in response
    if firmware not in response.text:
        logger.warning(f'Firmware "{firmware}" not found in Firmware Management API')
        return None

    # Firmware(s) Found, return it as a Dictionary
    found_firmware = json.loads(response.text)

    return found_firmware['values']


def imperium_trigger_firmware_update(device_id=config.DeviceID, target_firmware=config.fw_update, is_debug=False, scheduled_time="", update_type=2):
    """
    Triggers a Firmware Update to the desired Device
    By default, device_id is the DeviceID under test and the target firmware as well
    Returns the success of the request
    """

    url = f'https://{config.firmware_api_url}/platform/management/devices/api/v1/devices/{device_id}/firmware'

    body = {
        "version": target_firmware,
        "scheduledTime": scheduled_time,
        "debugVersion": is_debug,
        "tenant": None,
        "ioTHubName": None,
        "updateType": update_type
    }

    response = imperium_request(url=url, api='device_management_api', request_type='PUT', payload=json.dumps(body))

    # check if Dimming Curve was found
    if response.status_code != 200:
        logger.warning(f'Firmware update to {target_firmware} was not possible')
        return False

    return True


