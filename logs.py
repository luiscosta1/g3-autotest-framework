####################################################################################
# Filename:     logs.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Version:      1.0
#
# Filetype:     Library file
# Description:  Functions to support the overall logging of the program
# Status:       Under development
# TODO:         - Create a log file per test case loaded
####################################################################################
""" Contains the main logger for the program """

import logging
import time

"""
DEBUG:      Detailed information, typically of interest only when diagnosing problems.
INFO:       Confirmation that things are working as expected.
WARNING:    An indication that something unexpected happened, or indicative of some problem in the near future 
            (e.g. ‘disk space low’). The software is still working as expected.
ERROR:      Due to a more serious problem, the software has not been able to perform some function.
CRITICAL:   A serious error, indicating that the program itself may be unable to continue running.

LogRecord Attributes: https://docs.python.org/3/library/logging.html
"""

# This is the main logger. import it as "from logs import logger" and use it anywhere
logger = logging.getLogger(__name__)

# The logging level needs to be set as:
logger.setLevel("DEBUG")


formatter = logging.Formatter('%(asctime)s: %(funcName)s : %(levelname)s : %(message)s')

# Log to file handler
file_handler = logging.FileHandler('logs/logfile.log')
# file_handler.setLevel(LOG_LEVEL)
file_handler.setFormatter(formatter)

# Log to command line handler
stream_handler = logging.StreamHandler()
# stream_handler.setLevel(LOG_LEVEL)
stream_handler.setFormatter(formatter)

# Set handler properties
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

