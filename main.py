####################################################################################
# Filename:     main.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   26.05.2022
#
# Version:      3.0
#
# Filetype:     Main Application file
# Description:  Exedra G3 Automation Tool
# STATUS:       Stable
# Limitation:
####################################################################################
""" Main Application File to launch the Testing GUI """
import config
import g3_d2c_messages as d2c
from g3_c2d_messages import build_g3_c2d_message
import imperium

# TODO: Try import csv_processor
from csv_processor import *
from coap import create_coap_payload

import argparse
import eel
import sys
import platform
import threading
from logs import logger

selected_c2d_message = ''


def start_eel():
    """
    This Function starts the GUI and keeps Python running in Background
    """

    page = 'settings.html'
    eel.init("gui")

    try:
        # Tries to open app in Chrome
        eel.start(page, mode='chrome')
    except EnvironmentError:
        # If Chrome isn't found, fallback to Microsoft Edge on Win10 or greater
        if sys.platform in ['win32', 'win64'] and int(platform.release()) >= 10:
            eel.start(page, mode='edge', block=False)
        else:
            raise

    while True:
        # Loop to keep Python running
        # logger.debug('Python is running....')
        eel.sleep(2)


@eel.expose
def get_parameter(gui_parameter):
    """This function retrieves the requested parameter to the GUI"""

    if not type(gui_parameter) == str:
        logger.warning(f'Invalid Requested parameter: {gui_parameter} -> Not a String')
        return "Invalid"

    if gui_parameter == 'environment':
        logger.debug(f'returning {gui_parameter} = {config.environment} to GUI')
        return config.environment

    if gui_parameter == 'DeviceID':
        logger.debug(f'returning {gui_parameter} = {config.DeviceID} to GUI')
        return config.DeviceID

    if gui_parameter == 'Dummy DeviceID':
        logger.debug(f'returning {gui_parameter} = {config.device_dummy} to GUI')
        return config.device_dummy

    if gui_parameter == '1-10V DeviceID':
        logger.debug(f'returning {gui_parameter} = {config.device_1_10v} to GUI')
        return config.device_1_10v

    if gui_parameter == 'DALI DeviceID':
        logger.debug(f'returning {gui_parameter} = {config.device_dali} to GUI')
        return config.device_dali

    if gui_parameter == 'Multi-DALI DeviceID':
        logger.debug(f'returning {gui_parameter} = {config.device_multi_dali} to GUI')
        return config.device_multi_dali

    if gui_parameter == 'TW DeviceID':
        logger.debug(f'returning {gui_parameter} = {config.device_tw} to GUI')
        return config.device_tw

    if gui_parameter == 'RFID':
        logger.debug(f'returning {gui_parameter} = {config.AssetID} to GUI')
        return config.AssetID

    if gui_parameter == 'fw_illegal':
        logger.debug(f'returning {gui_parameter} = {config.fw_illegal} to GUI')
        return config.fw_illegal

    if gui_parameter == 'fw_update':
        logger.debug(f'returning {gui_parameter} = {config.fw_update} to GUI')
        return config.fw_update

    if gui_parameter == 'g3_cli_remote':
        logger.debug(f'returning {gui_parameter} = {config.g3_connection_settings["g3_cli_remote"]} to GUI')
        return config.g3_connection_settings["g3_cli_remote"]

    if gui_parameter == 'cli_port':
        logger.debug(f'returning {gui_parameter} = {config.g3_connection_settings["cli_port"]} to GUI')
        return config.g3_connection_settings["cli_port"]

    if gui_parameter == 'seco_host':
        logger.debug(f'returning {gui_parameter} = {config.g3_connection_settings["seco_host"]} to GUI')
        return config.g3_connection_settings["seco_host"]

    if gui_parameter == 'seco_port':
        logger.debug(f'returning {gui_parameter} = {config.g3_connection_settings["seco_port"]} to GUI')
        return config.g3_connection_settings["seco_port"]

    if gui_parameter == 'gps_lat':
        logger.debug(f'returning {gui_parameter} = {config.gps_lat} to GUI')
        return config.gps_lat

    if gui_parameter == 'gps_long':
        logger.debug(f'returning {gui_parameter} = {config.gps_long} to GUI')
        return config.gps_long

    if gui_parameter == 'timezone':
        logger.debug(f'returning {gui_parameter} = {config.timezone} to GUI')
        return config.timezone

    if gui_parameter == 'burn_hour_timeout':
        logger.debug(f'returning {gui_parameter} = {config.burn_hour_timeout} to GUI')
        return config.burn_hour_timeout

    if gui_parameter == 'temp_time_hyst':
        logger.debug(f'returning {gui_parameter} = {config.temp_time_hyst} to GUI')
        return config.temp_time_hyst

    if gui_parameter == 'temp_max':
        logger.debug(f'returning {gui_parameter} = {config.temp_max} to GUI')
        return config.temp_max

    if gui_parameter == 'start_up_seq_time':
        logger.debug(f'returning {gui_parameter} = {config.start_up_seq_time} to GUI')
        return config.start_up_seq_time

    if gui_parameter == 'photocell_enabled':
        logger.debug(f'returning {gui_parameter} = {config.photocell_enabled} to GUI')
        return config.photocell_enabled

    if gui_parameter == 'sensor_count':
        logger.debug(f'returning {gui_parameter} = {config.sensor_count} to GUI')
        return config.sensor_count

    if gui_parameter == 'imperium_bearer_token':
        logger.warning(f'No longer returning {gui_parameter} - To UI --- DELETE THIS')
        return 'Imperium Token - To be deleted'

    if gui_parameter in config.ACPower_errors_dict:
        logger.debug(f'returning {gui_parameter} = {config.ACPower_errors_dict.get(gui_parameter)} to GUI')
        return config.ACPower_errors_dict.get(gui_parameter)

    if gui_parameter == 'BrokenLamp':
        logger.debug(f'returning {gui_parameter} = {config.BrokenLamp} to GUI')
        return config.BrokenLamp

    if gui_parameter == 'DALIBallastStatus':
        logger.debug(f'returning {gui_parameter} = {config.DALIBallastStatus} to GUI')
        return config.DALIBallastStatus

    if gui_parameter == 'DALI101_Error':
        logger.debug(f'returning {gui_parameter} = {config.DALI101_Error} to GUI')
        return config.DALI101_Error

    if gui_parameter == 'ErrIPCComm':
        logger.debug(f'returning {gui_parameter} = {config.ErrIPCComm} to GUI')
        return config.ErrIPCComm

    if gui_parameter == 'DALIStatus':
        logger.debug(f'returning {gui_parameter} = {config.DALIStatus} to GUI')
        return config.DALIStatus

    if gui_parameter in config.BurningHours_dict:
        logger.debug(f'returning {gui_parameter} = {config.BurningHours_dict.get(gui_parameter)} to GUI')
        return config.BurningHours_dict.get(gui_parameter)

    if gui_parameter in config.dimfeedback_dict:
        logger.debug(f'returning {gui_parameter} = {config.dimfeedback_dict.get(gui_parameter)} to GUI')
        return config.dimfeedback_dict.get(gui_parameter)

    if gui_parameter == 'num_imported_devices':
        logger.debug(f'returning {gui_parameter} = {len(config.imported_devices_list)} to GUI')
        return len(config.imported_devices_list)

    if gui_parameter == 'sensor_type':
        logger.debug(f'returning {gui_parameter} = {config.sensor_type} to GUI')
        return config.sensor_type

    if gui_parameter == 'source_device_id':
        logger.debug(f'returning {gui_parameter} = {config.source_device_id} to GUI')
        return config.source_device_id

    if gui_parameter == 'sensor_hold_time':
        logger.debug(f'returning {gui_parameter} = {config.sensor_hold_time} to GUI')
        return config.sensor_hold_time

    if gui_parameter == 'sup_dim_feed':
        logger.debug(f'returning {gui_parameter} = {config.sup_dim_feed} to GUI')
        return config.sup_dim_feed
    #
    # if gui_parameter == 'environment':
    #     logger.debug(f'returning {gui_parameter} = {config.environment} to GUI')
    #     return config.environment

    logger.warning('Requested Parameter not found: ', gui_parameter)
    return "Not Found"


@eel.expose
def set_parameter(gui_parameter, gui_value):
    """
    This function receives the parameter to be set and the respective value as strings
    The Runtime Variables in config.py are updated herein
    Returns False if the input parameters are not valid
    """
    # Input Parameter must always be string
    if not type(gui_parameter) == str:
        logger.warning(f'Invalid Input Parameter: "{gui_parameter}" -> discarding ....')
        return False
    # Input Value must always be either string or Boolean
    if type(gui_value) != str and type(gui_value) != bool:
        logger.warning(f'Invalid Input Value: "{gui_value}" -> discarding ....')
        return False

    # Different parameters require different validations:

    # Check if it's the DeviceID
    if gui_parameter == "DeviceID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:    # Length Must be 16
            logger.warning(f'Invalid Device ID length: "{gui_value}" -> discarding ....')
            return False
        elif not gui_value.isalnum():   # Must be Alphanumerical
            logger.warning(f'Invalid Device ID string: "{gui_value}" -> discarding ....')
            return False
        else:
            config.DeviceID = gui_value.upper()
            logger.debug(f'Setting DeviceID {config.DeviceID} from GUI')
            return True

    # Check if it's the Dummy DeviceID
    if gui_parameter == "Dummy DeviceID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:    # Length Must be 16
            logger.warning(f'Invalid Device ID length: "{gui_value}" -> discarding ....')
            return False
        elif not gui_value.isalnum():   # Must be Alphanumerical
            logger.warning(f'Invalid Device ID string: "{gui_value}" -> discarding ....')
            return False
        else:
            config.device_dummy = gui_value.upper()
            logger.debug(f'Setting Dummy DeviceID {config.device_dummy} from GUI')
            return True

    # Check if it's the 1-10V DeviceID
    if gui_parameter == "1-10V DeviceID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:    # Length Must be 16
            logger.warning(f'Invalid Device ID length: "{gui_value}" -> discarding ....')
            return False
        elif not gui_value.isalnum():   # Must be Alphanumerical
            logger.warning(f'Invalid Device ID string: "{gui_value}" -> discarding ....')
            return False
        else:
            config.device_1_10v = gui_value.upper()
            logger.debug(f'Setting 1-10V DeviceID {config.device_1_10v} from GUI')
            return True

    # Check if it's the DALI DeviceID
    if gui_parameter == "DALI DeviceID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:    # Length Must be 16
            logger.warning(f'Invalid Device ID length: "{gui_value}" -> discarding ....')
            return False
        elif not gui_value.isalnum():   # Must be Alphanumerical
            logger.warning(f'Invalid Device ID string: "{gui_value}" -> discarding ....')
            return False
        else:
            config.device_dali = gui_value.upper()
            logger.debug(f'Setting DALI DeviceID {config.device_dali} from GUI')
            return True

    # Check if it's the Multi-DALI DeviceID
    if gui_parameter == "Multi-DALI DeviceID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:    # Length Must be 16
            logger.warning(f'Invalid Device ID length: "{gui_value}" -> discarding ....')
            return False
        elif not gui_value.isalnum():   # Must be Alphanumerical
            logger.warning(f'Invalid Device ID string: "{gui_value}" -> discarding ....')
            return False
        else:
            config.device_multi_dali = gui_value.upper()
            logger.debug(f'Setting Multi-DALI DeviceID {config.device_multi_dali} from GUI')
            return True

    # Check if it's the TW DeviceID
    if gui_parameter == "TW DeviceID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:    # Length Must be 16
            logger.warning(f'Invalid Device ID length: "{gui_value}" -> discarding ....')
            return False
        elif not gui_value.isalnum():   # Must be Alphanumerical
            logger.warning(f'Invalid Device ID string: "{gui_value}" -> discarding ....')
            return False
        else:
            config.device_tw = gui_value.upper()
            logger.debug(f'Setting Multi-DALI DeviceID {config.device_tw} from GUI')
            return True

    # Check if it's the RFID
    if gui_parameter == "RFID":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:  # Length Must be 16
            logger.warning(f'Invalid RFID length: "{gui_value}" -> discarding ....')
            return False
        elif not gui_value.isalnum():  # Must be Alphanumerical
            logger.warning(f'Invalid RFID string: "{gui_value}" -> discarding ....')
            return False

        config.RFID = gui_value.upper()
        logger.debug(f'Setting RFID {config.RFID} from GUI')
        return True

    # Check if it's the fw_illegal
    if gui_parameter == "fw_illegal":
        if len(gui_value) > 20:     # Are Firmware versions ever longer than 12?
            logger.warning(f'Invalid Illegal Firmware length: "{gui_value}" -> discarding ....')
            return False
        config.fw_illegal = gui_value
        logger.debug(f'Setting fw_illegal {config.fw_illegal} from GUI')
        return True

    # Check if it's the fw_update
    if gui_parameter == "fw_update":
        if len(gui_value) > 12:  # Are Firmware versions ever longer than 12?
            logger.warning(f'Invalid Firmware Update length: "{gui_value}" -> discarding ....')
            return False
        config.fw_update = gui_value
        logger.debug(f'Setting fw_update {config.fw_update} from GUI')
        return True

    # Check if it's the cli_port
    if gui_parameter == "cli_port":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) > 6:  # Max Length = 6
            logger.warning(f'Invalid G3 COM Port: "{gui_value}" -> discarding ....')
            return False
        elif not gui_value.isalnum():  # Must be Alphanumerical
            logger.warning(f'Invalid G3 COM Port: "{gui_value}" -> discarding ....')
            return False

        config.g3_connection_settings["cli_port"] = gui_value
        logger.debug(f'Setting cli_port {config.g3_connection_settings["cli_port"]} from GUI')
        return True

    # Check if it's the seco_host
    if gui_parameter == "seco_host":
        # Need to verify the input values. Return False if NOK
        if len(gui_value) > 32:  # Max Length = 32
            logger.warning(f'Invalid SeCo Hostname: "{gui_value}" -> discarding ....')
            return False
        # elif not gui_value.isalnum():  # Must be Alphanumerical
        #     print('Invalid SeCo Hostname: "', gui_value, '" -> discarding ....')
        #     return False

        config.g3_connection_settings["seco_host"] = gui_value
        logger.debug(f'Setting seco_host {config.g3_connection_settings["seco_host"]} from GUI')
        return True

    # Check if it's the seco_port
    if gui_parameter == "seco_port":
        # Need to verify the input values. Return False if NOK
        try:
            int_seco_port = int(gui_value)
        except ValueError:
            logger.warning(f'Invalid SeCo Port: "{gui_value}" -> discarding ....')
            return False

        if int_seco_port < 0:
            logger.warning(f'Invalid SeCo Port: "{gui_value}" -> discarding ....')
            return False

        config.g3_connection_settings["seco_port"] = gui_value
        logger.debug(f'Setting seco_port {config.g3_connection_settings["seco_port"]} from GUI')
        return True

    # Check if it's the gps_lat
    if gui_parameter == "gps_lat":
        # Need to verify the input values. Return False if NOK
        try:
            f_gui_lat = float(gui_value)
        except ValueError:
            logger.warning(f'Invalid GPS Latitude: "{gui_value}" -> discarding ....')
            return False

        # round to 6 decimal places
        f_gui_lat = round(f_gui_lat, 6)
        # set as a string
        config.gps_lat = str(f_gui_lat)
        logger.debug(f'Setting gps_lat {config.gps_lat} from GUI')
        return True

    # Check if it's the gps_long
    if gui_parameter == "gps_long":
        # Need to verify the input values. Return False if NOK
        try:
            f_gui_long = float(gui_value)
        except ValueError:
            logger.warning(f'Invalid GPS Longitude: "{gui_value}" -> discarding ....')
            return False

        # round to 6 decimal places
        f_gui_long = round(f_gui_long, 6)
        # set as a string
        config.gps_long = str(f_gui_long)
        logger.debug(f'Setting gps_long {config.gps_long} from GUI')
        return True

    # Check if it's the timezone
    if gui_parameter == "timezone":
        # Need to verify the input values. Return False if NOK
        try:
            int_timezone = int(gui_value)
        except ValueError:
            print('Invalid Timezone: "', gui_value, '" -> discarding ....')
            return False

        if int_timezone < -12 or int_timezone > 14:
            print('Invalid Timezone: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.timezone = gui_value
        print('Setting timezone ', config.timezone, ' from GUI')
        return True

    # Check if it's the burn_hour_timeout
    if gui_parameter == "burn_hour_timeout":
        # Need to verify the input values. Return False if NOK
        try:
            int_burn_hour_timeout = int(gui_value)
        except ValueError:
            print('Invalid Burning Hours Timeout: "', gui_value, '" -> discarding ....')
            return False

        if int_burn_hour_timeout < 0:
            print('Invalid Burning Hours Timeout: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.burn_hour_timeout = gui_value
        print('Setting Burning Hours Timeout ', config.burn_hour_timeout, ' from GUI')
        return True

    # Check if it's the temp_time_hyst
    if gui_parameter == "temp_time_hyst":
        # Need to verify the input values. Return False if NOK
        try:
            int_temp_time_hyst = int(gui_value)
        except ValueError:
            print('Invalid Temperature Time Hysteresis: "', gui_value, '" -> discarding ....')
            return False

        if int_temp_time_hyst < 0:
            print('Invalid Temperature Time Hysteresis: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.temp_time_hyst = gui_value
        print('Setting Temperature Time Hysteresis ', config.temp_time_hyst, ' from GUI')
        return True

    # Check if it's the temp_max
    if gui_parameter == "temp_max":
        # Need to verify the input values. Return False if NOK
        try:
            int_temp_max = int(gui_value)
        except ValueError:
            print('Invalid Maximum Temperature: "', gui_value, '" -> discarding ....')
            return False

        if int_temp_max < 0:
            print('Invalid Maximum Temperature: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.temp_max = gui_value
        print('Setting Maximum Temperature ', config.temp_max, ' from GUI')
        return True

    # Check if it's the start_up_seq_time
    if gui_parameter == "start_up_seq_time":
        # Need to verify the input values. Return False if NOK
        try:
            int_start_up_seq_time = int(gui_value)
        except ValueError:
            print('Invalid Startup Sequence Time: "', gui_value, '" -> discarding ....')
            return False

        if int_start_up_seq_time < 0:
            print('Invalid Startup Sequence Time: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.start_up_seq_time = gui_value
        print('Setting Startup Sequence Time ', config.start_up_seq_time, ' from GUI')
        return True

    # Check if it's the photocell_enabled
    if gui_parameter == "photocell_enabled":
        # Need to verify the input string. Return False if NOK
        if gui_value == 'true' or gui_value == 'false':
            config.photocell_enabled = gui_value
            print('Setting Startup Sequence Time ', config.photocell_enabled, ' from GUI')
            return True
        else:
            print('Invalid Photocell Enabled value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's the sensor_count
    if gui_parameter == "sensor_count":
        # Need to verify the input values. Return False if NOK
        try:
            int_sensor_count = int(gui_value)
        except ValueError:
            print('Invalid Sensor Counts: "', gui_value, '" -> discarding ....')
            return False

        if int_sensor_count < 0:
            print('Invalid Sensor Counts: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.sensor_count = gui_value
        print('Setting Sensor Counts ', config.sensor_count, ' from GUI')
        return True

    # Check if it's the g3_cli_remote
    if gui_parameter == 'g3_cli_remote':
        if type(gui_value) == bool:
            config.g3_connection_settings["g3_cli_remote"] = gui_value
            print('Setting g3_cli_remote ', config.g3_connection_settings["g3_cli_remote"], ' from GUI')
            return True
        else:
            print('Invalid g3_cli_remote Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's the Imperium Bearer Token
    if gui_parameter == "imperium_bearer_token":
        # Need to verify the input values. Return False if NOK
        # if len(gui_value) != 16:  # Length Must be 16
        #     print('Invalid RFID length: "', gui_value, '" -> discarding ....')
        #     return False
        # elif not gui_value.isalnum():  # Must be Alphanumerical
        #     print('Invalid RFID string: "', gui_value, '" -> discarding ....')
        #     return False

        config.imperium_bearer_token = gui_value
        print('Setting Imperium Token ', config.imperium_bearer_token, ' from GUI')
        return True

    # Check if it's an ACPower_error
    if gui_parameter in config.ACPower_errors_dict:
        if type(gui_value) == bool:
            # update the dictionary with the new value
            upd_param = {gui_parameter: gui_value}
            config.ACPower_errors_dict.update(upd_param)
            print('Setting ACPower_error ', gui_parameter, ' = ', config.ACPower_errors_dict[gui_parameter])
            return True
        else:
            print('Invalid ACPower_error Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a BrokenLamp_Error
    if gui_parameter == 'BrokenLamp':
        if type(gui_value) == bool:
            config.BrokenLamp = gui_value
            return True
        else:
            print('Invalid BrokenLamp Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a DALIBallastStatus_Error
    if gui_parameter == 'DALIBallastStatus':
        valid_inputs = ["", "LampArcPowerOn", "CommsError", "StatusOfControlGear", "LampFailure",
                        "LimitError", "FadeRunning", "ResetState", "PowerFailure", "ManualMode"]
        # if gui_value == 'LampArcPowerOn' or gui_value == 'CommsError':
        if gui_value in valid_inputs:
            config.DALIBallastStatus = gui_value
            print('Setting DALIBallastStatus to ', config.DALIBallastStatus, ' from GUI')
            return True
        else:
            print('Invalid DALIBallastStatus Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a DALI101_Error
    if gui_parameter == 'DALI101_Error':
        try:
            int_gui_value = int(gui_value)
        except ValueError:
            print('Invalid DALI101_Error: "', gui_value, '" -> discarding ....')
            return False

        if int_gui_value == 0 or int_gui_value == 128:
            config.DALI101_Error = int_gui_value
            print('Setting DALI101_Error to ', config.DALI101_Error, ' from GUI')
            return True
        else:
            print('Invalid DALI101_Error Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a ErrIPCComm
    if gui_parameter == 'ErrIPCComm':
        if type(gui_value) == bool:
            config.ErrIPCComm = gui_value
            return True
        else:
            print('Invalid ErrIPCComm Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a DALIStatus
    if gui_parameter == 'DALIStatus':
        valid_inputs = ["", "BusShort", "FrErr", "NoiseErr", "SeqErr", "BitErr", "ConflSAddr", "NewDev", "ZeroDev"]
        if gui_value in valid_inputs:
            config.DALIStatus = gui_value
            print('Setting DALIStatus_Error to ', config.DALIStatus, ' from GUI')
            return True
        else:
            print('Invalid DALIStatus_Error Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's the sensor_type
    if gui_parameter == 'sensor_type':
        if gui_value == 'local' or gui_value == 'OW3':
            config.sensor_type = gui_value
            print('Setting Sensor Type to ', config.sensor_type, ' from GUI')
            return True
        else:
            print('Invalid Sensor Type Input Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's the source_device_id
    if gui_parameter == 'source_device_id':
        # Need to verify the input values. Return False if NOK
        if len(gui_value) != 16:  # Length Must be 16
            print('Invalid Device ID length: "', gui_value, '" -> discarding ....')
            return False
        elif not gui_value.isalnum():  # Must be Alphanumerical
            print('Invalid Device ID string: "', gui_value, '" -> discarding ....')
            return False
        else:
            config.source_device_id = gui_value
            print('Setting Source Device ID to ', config.source_device_id, ' from GUI')
            return True

    # Check if it's the sensor_hold_time
    if gui_parameter == "sensor_hold_time":
        # Need to verify the input values. Return False if NOK
        try:
            int_sensor_hold_time = int(gui_value)
        except ValueError:
            print('Invalid Sensor Hold Time: "', gui_value, '" -> discarding ....')
            return False

        if int_sensor_hold_time < 0:
            print('Invalid Sensor Hold Time: "', gui_value, '" -> discarding ....')
            return False

        # set as a string
        config.sensor_hold_time = gui_value
        print('Setting Sensor Hold Time ', config.sensor_hold_time, ' from GUI')
        return True

    # Check if it's the sup_dim_feed
    if gui_parameter == 'sup_dim_feed':
        if gui_value == 'true' or gui_value == 'false':
            config.sup_dim_feed = gui_value
            print('Setting Suppress Dimfeedback to ', config.sup_dim_feed, ' from GUI')
            return True
        else:
            print('Invalid Suppress Dimfeedback Value: "', gui_value, '" -> discarding ....')
            return False

    # Check if it's a BurningHours value
    if gui_parameter in config.BurningHours_dict:
        # Need to verify the input values. Return False if NOK
        try:
            int_gui_value = int(gui_value)
        except ValueError:
            print('Invalid Burning Hours Value: "', gui_value, '" -> discarding ....')
            return False

        if int_gui_value < 0:
            print('Invalid Burning Hours Value: "', gui_value, '" -> discarding ....')
            return False

        # convert back to string
        gui_value = str(int_gui_value)
        # update the dictionary with the new value
        upd_param = {gui_parameter: gui_value}

        config.BurningHours_dict.update(upd_param)
        print('Setting Burning Hours ', gui_parameter, ' = ', config.BurningHours_dict[gui_parameter])
        return True

    # Check if it's a Dimfeedback value
    if gui_parameter in config.dimfeedback_dict:
        # Need to verify the input values. Return False if NOK
        try:
            # Check if it's convertible to float
            f_gui_value = float(gui_value)
        except ValueError:
            print('Invalid Dimfeedback Value: "', gui_value, '" -> discarding ....')
            return False

        # round to 3 decimal places
        f_gui_value = round(f_gui_value, 3)
        # convert back to string
        gui_value = str(f_gui_value)
        # update the dictionary with the new value
        upd_param = {gui_parameter: gui_value}

        config.dimfeedback_dict.update(upd_param)
        print('Setting Dimfeedback ', gui_parameter, ' = ', config.dimfeedback_dict[gui_parameter])
        return True

    # if none of the above, means the parameter does not exist
    print('Unknown Input values:  Parameter: ', gui_parameter, ';  Value: ', gui_value)
    return False


@eel.expose
def send_d2c_coap_together(queue_list, b_is_single_device, device_list=[]):
    """
    This Function receives a list of parameters that shall be sent in the via Coap, like:
    ['BurningHours', 'Dimfeedback_1','EnergyMeter']
    Encapsulates all in one queue and sends a single Coap Message
    If it's a single device (b_is_single_device), the target is the device defined in Settings
    If it's not a single device (b_is_single_device), the targets are the devices in the received List
    Returns False if no parameters are received, if they are invalid,
    or if the Coap message is not successfully sent
    """

    debug_num_devices = 0

    if len(queue_list) == 0:
        print("Queue is empty. Check input parameters. Coap message not Sent.")
        return False

    if b_is_single_device:
        # build the queue
        queue = d2c.build_g3_d2c_queue(queue_list, config.DeviceID)
        # build the payload
        message_body = d2c.build_g3_d2c_message(queue, config.DeviceID)

        # send the request
        print("Sending Device 2 Cloud Message with:\n", queue_list)

        response = d2c.send_d2c_exedra_coap(message_body)

        if response is None:
            print("Message not Acknowledged from Exedra: Too Many Elements sent together or Production environment?")
            return False
        elif "ACK" not in response.line_print:
            print("Message not Acknowledged from Exedra:", response)
            return False

        print('Coap message with ', queue_list, ' sent successfully! Response: ', response)

    else:  # Not a single device, so send the message from every device on the list
        for device in device_list[0]:
            # build the queue
            queue = d2c.build_g3_d2c_queue(queue_list, device.get("configuration.deviceId"))
            # build the payload
            message_body = d2c.build_g3_d2c_message(queue, device.get("configuration.deviceId"))

            # send the request
            response = d2c.send_d2c_exedra_coap(message_body)

            if response is None:
                print("Message not Acknowledged from Exedra: Too Many Elements sent together or Production environment?")
                return False
            elif "ACK" not in response.line_print:
                print("Message not Acknowledged from Exedra:", response)
                return False
            else:
                debug_num_devices += 1

    if not b_is_single_device:
        print("Successfully sent message to ", debug_num_devices, " devices")

    return True


@eel.expose
def send_d2c_custom_coap(payload, b_is_single_device, device_list=[]):
    """
    This Function receives a custom payload from the GUI that shall be sent in the via Coap
    Encapsulates all in one queue and sends a single Coap Message
    If it's a single device (b_is_single_device), the target is the device defined in Settings
    If it's not a single device (b_is_single_device), the targets are the devices in the received List
    Returns False if no payload is received, or if the Coap message is not successfully sent
    """

    debug_num_devices = 0

    if len(payload) == 0:
        print("Payload is empty. Check input parameters. Coap message not Sent.")
        return False

    if b_is_single_device:
        # build the full message payload
        message_body = d2c.build_g3_d2c_message(payload, config.DeviceID)

        # send the request
        print("Sending Device 2 Cloud Message:\n", payload)

        response = d2c.send_d2c_exedra_coap(message_body)

        if response is None:
            print("Message not Acknowledged from Exedra: Production environment?")
            return False
        elif "ACK" not in response.line_print:
            print("Message not Acknowledged from Exedra:", response)
            return False

        print('Coap message with ', payload, ' sent successfully! Response: ', response)

    else:  # Not a single device, so send the message from every device on the list
        for device in device_list[0]:
            # build the payload
            message_body = d2c.build_g3_d2c_message(payload, device.get("configuration.deviceId"))

            # send the request
            response = d2c.send_d2c_exedra_coap(message_body)

            if response is None:
                print("Message not Acknowledged from Exedra: Too Many Elements sent together or Production environment?")
                return False
            elif "ACK" not in response.line_print:
                print("Message not Acknowledged from Exedra:", response)
                return False
            else:
                debug_num_devices += 1

    if not b_is_single_device:
        print("Successfully sent message to ", debug_num_devices, " devices")

    return True


@eel.expose
def send_d2c_custom_coap_from_csv(payload):
    """
    This Function is triggered when the user selects "Send Custom message to All Devices" in the Device to Cloud Menu
    It creates a fixed number of threads, creating equally distributed lists and each thread calls
    send_d2c_custom_coap() with the payload
    Returns "True" when all the threads are finished
    """

    # need to separate the full device list through the threads
    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append(
            [config.imported_devices_list[devices_copied: (devices_copied + devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_d2c_custom_coap() function - REMOVE when not needed
    # send_d2c_custom_coap(payload, False, list_of_lists[thread])

    # Create the threads
    for thread in range(num_threads):

        new_thread = threading.Thread(target=send_d2c_custom_coap,
                                      args=(payload, False, list_of_lists[thread]))

        # append each thread handler to the threads_list
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return True


@eel.expose
def send_d2c_coap_separately(queue_list, b_is_single_device, device_list=[]):
    """
    This Function receives a list of parameters that shall be sent in the via Coap, like:
    ['BurningHours', 'Dimfeedback_1','EnergyMeter']
    It calls send_d2c_coap_together for every parameter separately
    Returns False if no parameters are received or if any Coap message is not successfully sent
    """

    if len(queue_list) == 0:
        print("Queue is empty. Check input parameters. Coap message not Sent.")
        return False

    # Iterate through all elements in the received queue_list and call send_d2c_coap_together() for each one
    for parameter in queue_list:
        parameter_as_list = [parameter]
        send_ok = send_d2c_coap_together(parameter_as_list, b_is_single_device, device_list)
        if not send_ok:
            print("Error Sending Coap Message with ", parameter)
            return False

    return True


@eel.expose
def send_d2c_coap_from_csv(queue_list, b_together):
    """
    This Function is triggered when the user selects "Send to All Devices" in the Device to Cloud MEnu
    It creates a fixed number of threads, creating equally distributed lists and each thread calls
    send_d2c_coap_together() or send_d2c_coap_separately() with a list of devices
    Returns "True" when all the threads are finished
    """

    # need to separate the full device list through the threads
    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append(
            [config.imported_devices_list[devices_copied: (devices_copied + devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_c2d_coap() function - REMOVE when not needed
    # send_d2c_coap_together(queue_list, False, list_of_lists[thread])
    # send_d2c_coap_separately(queue_list, False, list_of_lists[thread])

    # Create the threads
    for thread in range(num_threads):
        if b_together:
            new_thread = threading.Thread(target=send_d2c_coap_together,
                                          args=(queue_list, False, list_of_lists[thread]))
        else:
            new_thread = threading.Thread(target=send_d2c_coap_separately,
                                          args=(queue_list, False, list_of_lists[thread]))

        # append each thread handler to the threads_list
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return True


@eel.expose
def send_c2d_coap(b_is_single_device, device_list=[]):
    """
    This Function sends a Cloud to Device Message
    If it's a single device (b_is_single_device), the target is the device defined in Settings
    If it's not a single device (b_is_single_device), the targets are the devices in the received List
    By default, the device list is an Empty List, unless stated otherwise (b_is_single_device == False)
    Returns False if if the response is not 200 - OK
    """

    debug_num_devices = 0

    if b_is_single_device:
        # build the payload
        coap_payload = create_coap_payload(selected_c2d_message, config.DeviceID)

        # encapsulate the G3 message body inside the Imperium request payload
        message_body = build_g3_c2d_message(coap_payload)

        # send the request
        response = imperium.imperium_send_coap_to_device(message_body, config.DeviceID)

        if response.status_code != 202:
            print("Error Sending Message! Response Status Code:", response.content)
            return False

        print('Coap message with ', selected_c2d_message, ' sent successfully! Response: ', response)

    else:  # Not a single device, so send the message from every device on the list
        for device in device_list[0]:
            coap_payload = create_coap_payload(selected_c2d_message, device.get("configuration.deviceId"))

            # encapsulate the G3 message body inside the Imperium request payload
            message_body = build_g3_c2d_message(coap_payload)

            # send the request
            response = imperium.imperium_send_coap_to_device(message_body, device.get("configuration.deviceId"))
            print('\nImperium response:\n', response, response.content)

            if response.status_code != 202:
                print("Error Sending Message! Response Status Code:", response.content)
                return False
            else:
                debug_num_devices += 1

    if not b_is_single_device:
        print("Successfully sent message to ", debug_num_devices, " devices")

    return True


@eel.expose
def send_c2d_coap_from_csv():
    """
    This Function is triggered when the user selects "Send to All Devices" in the Cloud to Device Menu
    It creates a fixed number of threads, creating equally distributed lists and each thread calls
    send_c2d_coap() with a list of devices
    Returns "True" when all the threads are finished
    """

    # need to separate the full device list through the threads
    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append(
            [config.imported_devices_list[devices_copied: (devices_copied + devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_c2d_coap() function - REMOVE when not needed
    # send_c2d_coap(False, list_of_lists[thread])

    # Create the threads
    for thread in range(num_threads):

        new_thread = threading.Thread(target=send_c2d_coap,
                                      args=(False, list_of_lists[thread]))
        # append each thread handler to the threads_list
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return True


@eel.expose
def set_c2d_message(message):
    """
    This function sets the current Cloud to Device message selected by the user
    Returns False if the Selected message is not a valid string
    Returns True otherwise
    """

    global selected_c2d_message

    # Input Parameter must always be string
    if not type(message) == str or len(message) == 0:
        print('Invalid Input message: "', message, '" -> discarding ....')
        return False

    selected_c2d_message = message
    print('Selected Cloud 2 Device Message: ', selected_c2d_message)

    return True


@eel.expose
def send_c2d_custom_coap(b_is_single_device, g3_object, g3_index, custom_payload, device_list=[]):
    """
    This Function sends a Cloud to Device Custom Message
    If it's a single device (b_is_single_device), the target is the device defined in Settings
    If it's not a single device (b_is_single_device), the targets are the devices in the received List
    By default, the device list is an Empty List, unless stated otherwise (b_is_single_device == False)
    The g3_object, g3_index and custom_payload must be passed when calling this function
    Returns False if if the response is not 200 - OK
    """

    debug_num_devices = 0

    # build the payload
    coap_payload = [g3_object, g3_index, custom_payload]

    if b_is_single_device:
        # encapsulate the G3 message body inside the Imperium request payload
        message_body = build_g3_c2d_message(coap_payload)

        # send the request
        response = imperium.imperium_send_coap_to_device(message_body, config.DeviceID)

        if response.status_code != 202:
            print("Error Sending Message! Response Status Code:", response.content)
            return False

        print('Coap message with ', selected_c2d_message, ' sent successfully! Response: ', response)

    else:  # Not a single device, so send the message from every device on the list
        for device in device_list[0]:
            # encapsulate the G3 message body inside the Imperium request payload
            message_body = build_g3_c2d_message(coap_payload)

            # send the request
            response = imperium.imperium_send_coap_to_device(message_body, device.get("configuration.deviceId"))

            if response.status_code != 202:
                print("Error Sending Message! Response Status Code:", response.content)
                return False
            else:
                debug_num_devices += 1

    if not b_is_single_device:
        print("Successfully sent message to ", debug_num_devices, " devices")

    return True


@eel.expose
def send_c2d_custom_coap_from_csv(g3_object, g3_index, custom_payload):
    """
    This function is used to send the Custom Payload from the Web UI to all the devices loaded from the CSV file
    It creates the threads and splits the devices among them
    Each thread runs send_c2d_custom_coap() with a list of devices
    """

    # need to separate the full device list through the threads
    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append(
            [config.imported_devices_list[devices_copied: (devices_copied + devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_c2d_coap() function - REMOVE when not needed
    # send_c2d_custom_coap(False, g3_object, g3_index, custom_payload, list_of_lists[thread])

    # Create the threads
    for thread in range(num_threads):

        new_thread = threading.Thread(target=send_c2d_custom_coap,
                                      args=(False, g3_object, g3_index, custom_payload, list_of_lists[thread]))
        # append each thread handler to the threads_list
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return True


@eel.expose
def get_c2d_payload():
    """
    This function retrieves the payload of the currently selected message "selected_c2d_message" to the Web UI
    """

    payload = create_coap_payload(selected_c2d_message, config.DeviceID)

    return payload[2]


@eel.expose
def get_d2c_payload(queue_list):
    """
    This function retrieves the queue of the Device to Cloud message to be sent to the Web UI
    """
    if len(queue_list) == 0:
        print("Queue is empty. Returned empty Queue to GUI")
        return ""

    # build the queue
    queue = d2c.build_g3_d2c_queue(queue_list, config.DeviceID)

    return queue


@eel.expose
def change_environment(env):
    """
    This function changes the current environment to production, changing the required Runtime Variables
    Returns True if change was successful
    Returns False otherwise
    """

    if config.set_environment(env):
        save_current_settings()
        return True
    else:
        return False


@eel.expose
def save_current_settings():
    """
    This function saves the current environment settings to the settings.json file,
    calling config.save_current_settings() from config.py module.
    Returns True if save is successful
    Returns False if an error occurred
    """

    return config.save_current_settings()


if __name__ == '__main__':

    config.load_settings()

    parser = argparse.ArgumentParser()
    parser.add_argument("--loglevel",
                        help="Set the details to the desired loglevel. DEBUG, INFO (default), WARNING, ERROR")
    user_input_args = parser.parse_args()

    print(f'{user_input_args=}')

    if user_input_args.loglevel:
        logger.setLevel(user_input_args.loglevel)
    # Start the GUI
    start_eel()

    """ Code shall never reach this section as it will remain in a loop inside start_eel()
        Use the section below to perform some validation tests """
    ##################################################################

    ##################################################################
