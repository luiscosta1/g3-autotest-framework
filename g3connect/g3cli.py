####################################################################################
# Filename:     g3cli.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   21.05.2022
#
# Version:      2.0
#
# Filetype:     Library file
# Description:  Functions to support local reading and writing G3 CLI and Logs
# Status:       Under development
# Limitation:   File must be in the same location as scripts
# TODO:         - fix issue importing serial
#               - Re-think serial port opening
#               - make cli as an object - local_cli
####################################################################################
"""Functions to support LOCAL reading and writing G3 CLI and Logs"""

import serial
##from serial import serialcli as Serial
import datetime
import time
import config
from logs import logger

# Declare serial ports as global variables, but do not open them
serial_cli = serial.Serial()
serial_cli.port = config.cli_port
serial_cli.baudrate = config.cli_baudrate
serial_cli.timeout = config.cli_timeout

serial_logs = serial.Serial()
serial_logs.port = config.cli_logs_port
serial_logs.baudrate = config.cli_baudrate
serial_logs.timeout = config.cli_timeout

serial_binary = serial.Serial()
serial_binary.port = config.cli_binary_port
serial_binary.baudrate = config.cli_baudrate
serial_binary.timeout = config.cli_timeout


def open_serial_port(port):
    """
    Opens the specified serial port
    """

    if port == 'cli':
        if not serial_cli.isOpen():
            serial_cli.open()
        # Check if Open() was successful
        if serial_cli.isOpen():
            return True
        else:
            logger.error(f"Failed to Open Local {port} Port")
            return False

    elif port == 'logs':
        if not serial_logs.isOpen():
            serial_logs.open()
        # Check if Open() was successful
        if serial_logs.isOpen():
            return True
        else:
            logger.error(f"Failed to Open Local {port} Port")
            return False

    elif port == 'binary':
        if not serial_binary.isOpen():
            serial_binary.open()
        # Check if Open() was successful
        if serial_binary.isOpen():
            return True
        else:
            logger.error(f"Failed to Open Local {port} Port")
            return False

    else:
        logger.error(f"Unknown Port: {port}")
        return False


def close_serial_port(port):
    """
    Closes the specified serial port
    """
    if port == 'cli':
        serial_cli.close()
    elif port == 'logs':
        serial_logs.close()
    elif port == 'binary':
        serial_binary.close()
    else:
        return  # nothing to do


def write_to_cli(cli_command) -> int:
    """
    Writes command to G3 CLI
    Returns number of bytes written
    """

    # first, append \r (Enter) character to the command
    cli_command = cli_command + '\r'

    if not serial_cli.isOpen():
        serial_cli.open()

    cli_command = cli_command.encode()
    bytes_written = serial_cli.write(cli_command)
    # serial_cli.flush()
    serial_cli.send_break(0.5)

    return bytes_written


def read_from_cli() -> str:
    """
    Reads command from G3 CLI
    Returns response
    """

    if not serial_cli.isOpen():
        serial_cli.open()

    data = serial_cli.read_all().decode('UTF-8')
    data = trim_g3_response(data)
    return data


def cli_g3write(cli_command) -> int:
    """
    Writes command to G3 CLI and reads the command response
    Returns the response from the G3 Controller
    """
    write_to_cli(cli_command)

    response = read_from_cli()

    return response


def trim_g3_response(data) -> str:
    """
    This is a auxiliary function, used to
    cut the received G3 response and returns only the desired response
    eg. Raw format: '\nRESPONSE\n[CLI]>' -> trimmed_response = RESPONSE
    """

    trimmed_response = data.strip("[CLI]>")
    trimmed_response = trimmed_response.strip('\n')  # remove both '\n'

    return trimmed_response


############ Below functions shall only be used in Local Mode ############

def read_from_g3_logs(max_characters) -> str:
    """
    Reads command from G3 logs until max_characters argument is reached
    Returns the response
    """

    data = ''
    if not serial_logs.isOpen():
        serial_logs.open()

    while True:
        #  Append read data to the buffer until it reaches max_characters
        data += serial_logs.readline().decode('UTF-8')
        if len(data) > max_characters:
            break
    return data


def wait_g3_cli_ready() -> bool:
    """
    After G3 Reboot
    Wait for console until [CLI]> will appear at the new line.
    This means it is ready to start receiving commands
    Returns True when '[CLI]>' is found in the buffer.
    Returns False if for 5 minutes it didn't show up
    ********  NOT YET READY  ********
    """

    data = ''
    while not serial_cli.isOpen():  # Restart closes COM Port. We need to wait and retry until it re-opens
        serial_cli.open()
        time.sleep(1.0)
        continue

    starttime_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=5)

    # Port is now open.
    while datetime.datetime.now() < starttime_plus_5:   # for a max of 5 minutes
        data += serial_cli.read_all().decode('UTF-8')   # check if '[CLI]>' shows up in the cli
        if '[CLI]>' not in data:
            time.sleep(1.0)
            continue
        else:
            logger.debug('CLI is ready to go!')
            return True
    # Something went wrong
    logger.warning('CLI was not ready in 5 minutes')
    return False


#
# def write_g3_coap(coap_msg) -> bool:
#     """
#     Writes coap message to G3 CLI
#     Returns True if Success (read OK from CLI after sending command)
#     """
#
#     data = 'coap ' + coap_msg[0] + '/' + coap_msg[1] + ', ' + coap_msg[2]
#     logger.debug(f'Writing {data}')
#
#     write_to_cli(data)
#
#     response = read_from_cli()
#
#     if '[COAP] OK' in response:
#         return True
#     else:
#         logger.error('[COAP] Error Writing COAP message to G3 CLI')
#         return False



