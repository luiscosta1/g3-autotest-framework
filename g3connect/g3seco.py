####################################################################################
# Filename:     g3seco.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   21.05.2022
#
# Version:      2.0
#
# Filetype:     Library file
# Description:  Functions to support reading and writing from G3 devices via SeCo
# Status:       Stable
# Limitation:
####################################################################################
"""The class G3Seco supports reading and writing from G3 devices via SeCo"""

import telnetlib
import time
from logs import logger

SECO_HOLD_TIME = 3


class G3Seco:
    def __init__(self, g3_settings, device_id=None):
        self.device_id = device_id
        try:
            self.seco_host = g3_settings["seco_host"]
            self.seco_port = g3_settings["seco_port"]
            self.seco_timeout = g3_settings["seco_timeout"]
            self.seco_user = g3_settings["seco_user"]
            self.seco_password = g3_settings["seco_password"]
            # to be removed:
            logger.debug(f'{self.seco_host=}')
            logger.debug(f'{self.seco_port=}')
            logger.debug(f'{self.seco_timeout=}')
            logger.debug(f'{self.seco_user=}')
            logger.debug(f'{self.seco_password=}')
        except KeyError:
            logger.error("Error Loading G3 SeCo settings")
            return

        """ Create the SeCo telnet instance, but do not open the connection
        This is the telnet connection handler we will use on the G3Seco methods"""
        self.seco = telnetlib.Telnet()

        """ if G3Seco was created with a device id as arguments (e.g. g3 = G3Seco(g3_settings, device_id)  )
        we will initialize the connection straight away:"""
        if device_id is not None:
            self.seco_connect(device_id)

    def seco_connect(self, DeviceID):
        """
        This function Sets Up a Telnet connection to the SeCo
        Returns True ONLY IF the connection is successful AND no one else is using the SeCo Python Script
        Returns False if Connection is unsuccessful OR the OW3_CLI.py script failed to load in the SeCo
        """

        # Attempt SeCo Connection. Return False if not successful
        try:
            self.seco = telnetlib.Telnet(self.seco_host, port=self.seco_port, timeout=self.seco_timeout)
        except Exception as exception:
            logger.error(f'Communication to SeCo Unsuccessful. Exception: {exception}')
            return False

        logger.debug(f'Communication to SeCo Successful. Logging in:')
        read = self.seco.read_until(b"login: ", timeout=self.seco_timeout)
        self.seco_write(self.seco_user)
        logger.debug(f'{read} {self.seco_user}')

        read = self.seco.read_until(b"password: ", timeout=self.seco_timeout)
        self.seco_write(self.seco_password)
        logger.debug(f'{read} {self.seco_password}')

        # Starting SeCo python Script to Communicate with G3
        self.seco_write("py OW3_CLI.py")
        logger.debug("py OW3_CLI.py")

        read = self.seco.read_until(b"OW3_CLI (`!help` for help)", timeout=self.seco_timeout)
        if b"failed" in read:
            logger.error("Failed to Start G3 Communication: SeCo is already in use!!!")
            self.seco_close()
            return False

        # Now we need to start communicating to the Device Under Test
        command = '!a'
        for i in range(8):
            command += DeviceID[2*i:2*i+2] + ':'
            # Remove trailing ':'
        command = command[:-1]

        self.seco_write(command)

        read = self.seco.read_until(b"OW3_CLI (`!help` for help)>", timeout=self.seco_timeout)

        if b"help)>" not in read:
            # return False
            raise EnvironmentError

        return True

    def seco_close(self):
        """
        This function closes the OW3_CLI.py script in the SeCo and terminates the Telnet Connection
        Returns False if the script was not successfully closed
        Returns True otherwise
        """

        # Exiting SeCo python Script
        self.seco_write("!exit")

        read = self.seco.read_until(b"#> ", timeout=self.seco_timeout)
        if b"#> " not in read:
            return False

        # Exiting SeCo connection
        self.seco_write("exit")
        # close telnet connection. This is probably redundant as SeCo cuts the connection with the command above
        self.seco.close()

        return True

    def seco_write(self, string=''):
        """
        This function writes the received string to the Seco
        """

        # Append '\n' to confirm the command
        string += '\n'

        # write via telnet lib
        self.seco.write(string.encode('ascii'))
        time.sleep(SECO_HOLD_TIME)

    def seco_read(self):
        """
        This function reads the buffer in the SeCo
        Returns the response
        """

        # read via telnet lib
        response = self.seco.read_very_eager()

        response = trim_seco_response(response)

        return response

    def seco_g3write(self, string=''):
        """
        This function writes the received string to the G3 CLI via Seco and returns the response
        Typical reponses:
            OK
            [COAP] OK
            [TIME]
            or the value of the datapoint that was read
        """

        # write via telnet lib
        self.seco_write(string)
        response = self.seco_read()

        return response


def trim_seco_response(data) -> str:
    """
    This is a auxiliary function, used to
    cut the received G3 response and returns only the desired response
    eg. Raw format: '\nRESPONSE\n[CLI]>' -> trimmed_response = RESPONSE
    """

    # convert from byte array to string
    data = data.decode("ascii")

    if "\r\n\r\n" in data: # This is a response from the CLI via OW3_CLI.py
        # First we discard everything until "\r\n\r\n"
        trimmed_response = data[data.find("\r\n\r\n") + 4: ] # 4 = len("\r\n\r\n")

        # Then we discard everything after "\r\n"
        trimmed_response = trimmed_response[:trimmed_response.find("\r\n")]

        return trimmed_response
    else: # This is not a response from OW3_CLI.py
        return data


if __name__ == "__main__":

    g3_connection_settings = {
        "g3_cli_remote": True,
        "seco_host": "hyperiontest.ddns.net",
        "seco_port": "60913",
        "seco_timeout": 10,
        "seco_user": "root",
        "seco_password": "nightshift",
        "cli_port": "COM16",
        "cli_logs_port": "COM20",
        "cli_binary_port": "COM18",
        "cli_baudrate": 115200,
        "cli_databits": 8,
        "cli_stopbits": 1,
        "cli_parity": 'None',
        "cli_timeout": 1
    }

    """import as:   from g3connect.g3seco import G3Seco"""

    """ use this"""
    # g3 = G3Seco(g3_connection_settings)
    # g3.seco_connect("0013A20041BC6DF1")

    """or directly with Device ID, which will trigger connection to device:"""
    g3 = G3Seco(g3_connection_settings, device_id="0013A20041BC6DF1")

    cmd = 'set gps ' + str(38.888) + ' ' + str(-9.999) + ' ' + str(1)  # set gps <lat> <long> <accuracy>

    """This is how the write command needs to be used from above. it returns the G3 response already"""
    g3_response = g3.seco_g3write(cmd)
    print(f"Response to Write Command: {g3_response}")

    if 'OK' not in g3_response:
        logger.warning(f'Could not write gps position in device: {cmd=}')

    # read the coordinates back from the device
    latitude = g3.seco_g3write("read dp PositionTime.POSLAT")
    print(f"{latitude=}")
    longitude = g3.seco_g3write("read dp PositionTime.POSLON")
    print(f"{longitude=}")

    # Need to close the connection, otherwise next open attempt will fail
    g3.seco_close()

