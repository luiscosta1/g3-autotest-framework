####################################################################################
# Filename:     g3driver.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   21.05.2022
#
# Version:      2.0
#
# Filetype:     Library file
# Description:  Wrapper functions to support reading and writing from G3 CLI via SeCo or direct Serial port
# Status:       Under development
# Limitation:
# TODO:         - Conflict between cli.write_g3_coap() and cli_wrapper.write_g3_coap()
#               - Conflict between cli.wait_g3_cli_ready() and cli_wrapper.wait_g3_cli_ready()
####################################################################################
"""The class G3Driver supports reading and writing from G3 CLI via SeCo or direct Serial port
    It is an abstraction layer between the tester and the Physical G3 Connection"""

import time
import datetime
from retry import retry
from g3connect.g3seco import G3Seco
# from g3connect.g3cli import G3Cli
#import g3connect.g3cli

import coap
import g3connect.g3datapoints as g3datapoints
from logs import logger


class G3Driver:
    """
    shall receive a dictionary with the following 11 parameters:
    "g3_cli_remote"
    via SeCo: "seco_host", "seco_port", "seco_timeout", "seco_user", "seco_password"
    via Local CLI: "cli_port", "cli_logs_port", "cli_binary_port",
                    "cli_baudrate", "cli_databits", "cli_stopbits", "cli_parity", "cli_timeout",
    """

    def __init__(self, g3_settings):

        # Let's validate all input parameters and guarantee all are OK
        if type(g3_settings) is not dict:
            return

        try:
            self.is_remote = g3_settings["g3_cli_remote"]
        except KeyError:
            logger.error("Error Loading G3 CLI settings")
            return

        if self.is_remote:
            try:
                logger.debug("Using CLI via SeCo")
                self.g3 = G3Seco(g3_settings)
            except KeyError:
                logger.error("Error Loading G3 SeCo settings")
                return
        else:
            try:
                logger.debug("Using CLI via Serial Port")
                self.cli_port = g3_settings["cli_port"]
                self.cli_logs_port = g3_settings["cli_logs_port"]
                self.cli_binary_port = g3_settings["cli_binary_port"]
                self.cli_baudrate = g3_settings["cli_baudrate"]
                self.cli_databits = g3_settings["cli_databits"]
                self.cli_stopbits = g3_settings["cli_stopbits"]
                self.cli_parity = g3_settings["cli_parity"]
                self.cli_timeout = g3_settings["cli_timeout"]
                # TODO: create the CLI instance here, like self.g3 = G3Cli(g3_settings)
                self.g3 = G3Cli(g3_settings)
                # to be removed
                logger.debug(f'{self.cli_port=}')
                logger.debug(f'{self.cli_logs_port=}')
                logger.debug(f'{self.cli_binary_port=}')
                logger.debug(f'{self.cli_baudrate=}')
                logger.debug(f'{self.cli_databits=}')
                logger.debug(f'{self.cli_stopbits=}')
                logger.debug(f'{self.cli_parity=}')
                logger.debug(f'{self.cli_timeout=}')
            except KeyError:
                logger.error("Error Loading G3 Local CLI settings")
                return

    @retry(EnvironmentError, 4, 2, 1)
    def open_g3_cli(self, DeviceID) -> bool:
        """
        Opens communication to the G3 CLI via Seco if g3_cli_remote == True.
        Otherwise, it opens the local CLI on the COM debug port
        Returns True if Successful
        """

        if self.is_remote:
            success = self.g3.seco_connect(DeviceID)
        else:
            # success = cli.open_serial_port('cli')
            # TODO: future implementation:
            success = self.g3.open_serial_port('cli')

        if success:
            return True
        else:
            return False

    def close_g3_cli(self):
        """
        Closes the communication with the G3 CLI via Seco if g3_cli_remote == True.
        Otherwise, it closes the local CLI on the COM debug port
        Returns True if Successful
        """

        if self.is_remote:
            self.g3.seco_close()
        else:
            # cli.close_serial_port('cli')
            # TODO: future implementation:
            self.g3.close_serial_port('cli')

    def set_g3_time(self, str_time):
        """
        Writes the received time in G3 CLI via Seco if is_remote == True. Otherwise, writes via Local CLI.
        This function shall be called with config.now_UTC() as an argument
        time shall be specified in YYMMDDhhmmss format. Otherwise, response will NOT be OK
        Returns False if response does not contain '[TIME]'
        """
        command = 'time ' + str_time

        if self.is_remote:
            response = self.g3.seco_g3write(command)
        else:
            # cli.write_to_cli(command)
            # response = cli.read_from_cli()
            # TODO: future implementation:
            response = self.g3.cli_g3write(command)

        if response is None:
            logger.warning(f'Could not write time in device: {command=}')
            return False
        elif '[TIME]' not in response:
            logger.warning(f'Could not write time in device: {command=}')
            return False

        return True

    def write_g3_coap(self, coap_message, DeviceID) -> bool:
        """
        Sends a COAP message to the G3 CLI via Seco if is_remote == True. Otherwise, writes via Local CLI.
        Returns False if the write fails
        """
        # Build the coap message, with the correct timestamp and DeviceID
        coap_msg = coap.create_coap_payload(coap_message, DeviceID)

        command = 'coap ' + coap_msg[0] + '/' + coap_msg[1] + ', ' + coap_msg[2]
        logger.debug(f'Writing {command}')

        if self.is_remote:
            response = self.g3.seco_g3write(command)
        else:
            # cli.write_to_cli(command)
            # response = cli.read_from_cli()
            # TODO: future implementation:
            response = self.g3.cli_g3write(command)

        if response is None:
            logger.warning('[COAP] Error Writing COAP message to G3 CLI')
            return False
        elif '[COAP] OK' not in response:
            logger.warning('[COAP] Error Writing COAP message to G3 CLI')
            return False

        return True

    def set_g3_gps(self, latitude, longitude, accuracy=1):
        """
        Writes the received coordinates in G3 CLI
        If not specified, accuracy is set to 1 by default
        Returns False if response is not 'OK'
        """

        command = 'set gps ' + str(latitude) + ' ' + str(longitude) + ' ' + str(
            accuracy)  # set gps <lat> <long> <accuracy>

        if self.is_remote:
            response = self.g3.seco_g3write(command)
        else:
            # cli.write_to_cli(command)
            # response = cli.read_from_cli()
            # TODO: future implementation:
            response = self.g3.cli_g3write(command)

        if 'OK' not in response:
            logger.warning(f'Could not write gps position in device: {command=}')
            return False

        return True

    def read_datapoint(self, datapoint, instance=1) -> str:
        """
        Receives Datapoint to be read
        Returns response

        Info: Lamps contain max of 8 instances. Sensors contain max of 32 instances -> defined in resources_g3specific.py
        """

        obj = find_datapoint_in_g3_object(datapoint)

        if obj == '':  # datapoint not found in objects list
            return 'DATAPOINT NOT FOUND'

        # check validity of input parameters:
        if obj == 'Sensor':
            if instance > g3datapoints.MAX_SENSOR_INSTANCES:
                logger.error(f'Sensor cannot exceed {g3datapoints.MAX_SENSOR_INSTANCES} instances')
                return ''
        elif obj == 'Lamp':
            if instance > g3datapoints.MAX_LAMP_INSTANCES:
                logger.error(f'Lamp cannot exceed {g3datapoints.MAX_SENSOR_INSTANCES} instances')
                return ''

        # build command to send to device
        command = 'read dp ' + obj + '.' + str(instance) + '.' + datapoint

        if self.is_remote:
            response = self.g3.seco_g3write(command)
        else:
            # cli.write_to_cli(command)
            # response = cli.read_from_cli()
            # TODO: future implementation:
            response = self.g3.cli_g3write(command)

        return response

    def write_datapoint(self, datapoint, value, instance=1) -> bool:
        """
        Writes Datapoint.      eg.: write dp Lamp.1.MinVoltage=1
        Returns True if Success (read OK from CLI after sending command)
        """

        obj = find_datapoint_in_g3_object(datapoint)

        # build command to send
        command = 'write dp ' + obj + '.' + str(instance) + '.' + datapoint + '=' + str(value)

        if self.is_remote:
            response = self.g3.seco_g3write(command)
        else:
            # cli.write_to_cli(command)
            # response = cli.read_from_cli()
            # TODO: future implementation:
            response = self.g3.cli_g3write(command)

        if 'OK' not in response:
            logger.warning(f'Device did not respond "OK" to "{command}"')
            return False

        return True

    def wait_g3_cli_ready(self, timeout) -> bool:
        """
        After G3 Reboot
        Wait for console until [CLI]> will appear at the new line.
        This means it is ready to start receiving commands
        Returns True when '[CLI]>' is found in the buffer.
        Returns False if for 5 minutes it didn't show up
        ********  TODO: NOT YET TESTED  ********
        """

        if self.is_remote:
            # TODO: on device restart, seco cli connection is not broken
            # implement a while loop, asking the Version from the device until it responds
            starttime_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=timeout)

            while datetime.datetime.now() < starttime_plus_5:  # for a max of 5 minutes
                response = self.g3.seco_g3write('version')

                if 'VERSION' not in response:
                    time.sleep(2)
                    continue
                else:
                    logger.debug('CLI is ready to go!')
                    return True
            # Something went wrong
            logger.warning(f'CLI was not ready in {timeout} minutes')

            return False
        else:
            # TODO: Probably here we will call self.g3.wait_g3_cli_ready() as the code below is too low-level for the driver
            data = ''
            while not self.g3.serial_cli.isOpen():  # Restart closes COM Port. We need to wait and retry until it re-opens
                self.g3.serial_cli.open()
                time.sleep(1.0)
                continue

            starttime_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=timeout)

            # Port is now open.
            while datetime.datetime.now() < starttime_plus_5:  # for a max of 5 minutes
                data += self.g3.serial_cli.read_all().decode('UTF-8')  # check if '[CLI]>' shows up in the cli
                if '[CLI]>' not in data:
                    time.sleep(1.0)
                    continue
                else:
                    logger.debug('CLI is ready to go!')
                    return True
            # Something went wrong
            logger.warning(f'CLI was not ready in {timeout} minutes')
            return False

    def read_from_g3_logs(self, max_characters) -> str:
        """
        Reads command from G3 logs until max_characters argument is reached
        Returns the response
        """

        if self.is_remote:
            logger.error("G3 CLI logs are not available via SeCo")
            return ''
        else:
            data = ''
            if not self.g3.serial_logs.isOpen():
                self.g3.serial_logs.open()

            while True:
                #  Append read data to the buffer until it reaches max_characters
                data += self.g3.serial_logs.readline().decode('UTF-8')
                if len(data) > max_characters:
                    break
            return data

# To be removed. this is specific to local CLI
# def trim_g3_response(data) -> str:
#     """
#     This is a auxiliary function, used to
#     cut the received G3 response and returns only the desired response
#     eg. Raw format: '\nRESPONSE\n[CLI]>' -> trimmed_response = RESPONSE
#     """
#
#     trimmed_response = data.strip("[CLI]>")
#     trimmed_response = trimmed_response.strip('\n')  # remove both '\n'
#
#     return trimmed_response


def find_datapoint_in_g3_object(datapoint) -> str:
    """
    This is a auxiliary function, which
    Receives the datapoint to be read
    Returns the object which the datapoint belongs to
    eg. DeviceID datapoint belongs to G3_Controller object
    """

    # go through lists defined in g3datapoint_resources and return the object
    if datapoint in g3datapoints.Controller:
        obj = 'Controller'
    elif datapoint in g3datapoints.Lamp:
        obj = 'Lamp'
    elif datapoint in g3datapoints.PositionTime:
        obj = 'PositionTime'
    elif datapoint in g3datapoints.ACPower:
        obj = 'ACPower'
    elif datapoint in g3datapoints.Comms:
        obj = 'Comms'
    elif datapoint in g3datapoints.Management:
        obj = 'Management'
    elif datapoint in g3datapoints.Sensor:
        obj = 'Sensor'
    elif datapoint in g3datapoints.CLDriver:
        obj = 'CLDriver'
    else:
        obj = ''
        logger.warning(f'Datapoint "{datapoint}" not found in objects list')

    return obj


if __name__ == "__main__":

    g3_connection_settings = {
        "g3_cli_remote": True,
        "seco_host": "hyperiontest.ddns.net",
        "seco_port": "60913",
        "seco_timeout": 10,
        "seco_user": "root",
        "seco_password": "nightshift",
        "cli_port": "COM16",
        "cli_logs_port": "COM20",
        "cli_binary_port": "COM18",
        "cli_baudrate": 115200,
        "cli_databits": 8,
        "cli_stopbits": 1,
        "cli_parity": 'None',
        "cli_timeout": 1
    }

    g3 = G3Driver(g3_connection_settings)
    g3.open_g3_cli("0013A20041BC6DF1")

    g3.set_g3_gps(38.888, -9.444)

    lat = g3.read_datapoint("POSLAT")
    print(f"{lat=}")
    long = g3.read_datapoint("POSLON")
    g3.close_g3_cli()
    print(f"{long=}")
