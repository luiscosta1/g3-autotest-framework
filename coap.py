####################################################################################
# Filename:     coap.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Version:      1.0
#
# Filetype:     Resource file
# Description:  Coap messages and functions for Cloud to Device communication
# Status:       Stable
# Limitation:
####################################################################################
""" Coap messages and functions for Cloud to Device communication """

from enum import Enum
import datetime
import config
from logs import logger


# G3 Datapoint Objects
class G3objects(Enum):
    G3_Controller = 1
    G3_Lamp = 2
    G3_PositionTime = 3
    G3_ACPower = 4
    G3_Comms = 5
    G3_DataInfo = 6
    G3_Management = 7
    G3_Sensor = 8
    G3_CLDriver = 9


# Get current time + 5 min in "ss mm HH DD MM * YYYY" format. Used for manual dimming profile
now_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=5)
UTC_TIME_plus_5min = now_plus_5.strftime("%S %M %H %d %m * %Y")


def get_now_plus(value):
    """
    This function receives a value to add in minutes to the actual current time
    this is useful for manual dimming commands
    Returns the time formatted to use in the manual dimming override
    """
    now_plus = datetime.datetime.now() + datetime.timedelta(minutes=value)
    utc_time_plus = now_plus.strftime("%S %M %H %d %m * %Y")
    return utc_time_plus


def create_coap_payload(coap, device_id):
    """
    This function receives a keyword with the desired coap and a device ID
    Returns the COAP message as ['object', 'index', 'coap_payload'] with the device ID already embedded
    """

    if coap == 'fact_reset':
        coap_message = ['1', '1', '{"Ctrl":{"UTC":"' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ContID":8,"Queue":[{"Priority":64,"FACTdef":true}]}}']
        return coap_message
    elif coap == 'sw_reset':
        coap_message = ['1', '1', '{"Ctrl":{"UTC":"' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ContID":8,"Queue":[{"SwReset":true}]}}']
        return coap_message
    elif coap == 're_register':
        coap_message = ['1', '1', '{"Ctrl":{"UTC":"' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ContID":8,"Queue":[{"Priority":64,"Re-register":true}]}}']
        return coap_message
    elif coap == 'dali_reinit':
        coap_message = ['1', '1', '{"Ctrl":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ContID":2,"Queue":[{"Priority":64,"DALIre-init":true}]}}']
        return coap_message
    elif coap == 'debug_info':
        coap_message = ['1', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":8,"Queue":[{"Priority":64,"DebugEnable":true}]}}']
        return coap_message
    elif coap == 'read_rfid':
        coap_message = ['6', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":20,"Queue":[{"Priority":0,"QueueTarget":0,"Source":"Controller","Id":"RFIDID",\
        "SendPriority": "+00:00","QE":"Normal","Valid":{"Set":{"S":"true"},"O":{"T":"OS"}}}]}}']
        return coap_message
    elif coap == 'single_dim_feedback':
        coap_message = ['6', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":20,"Queue":[{"Priority": 0,"QueueTarget": 0,"Source": "Lamp","Index": 1,"Id": "DimFeedback",\
        "SendPriority": "+00:00","QE": "Normal","Valid": {"Set":{"S":"true"},"O":{"T":"OS"}}}]}}']
        return coap_message
    elif coap == 'loc_sen_en':
        coap_message = ['1', '1', '{"Cfg": {"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 13,"Queue": [{"Priority": 64,"LocSenEn": true,"RampDOWN": 200}]}}']
        return coap_message
    elif coap == 'burn_hour_timeout':
        coap_message = ['1', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":3,"Queue":[{"Priority":7,"BurningHourTimeout":' + config.burn_hour_timeout + '}]}}']
        return coap_message
    elif coap == 'high_temp_hyst':
        coap_message = ['1', '1', '{"Cfg":{"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID":8,"Queue": [{"Priority": 65,"TemperatureTimeHyst": ' + config.temp_time_hyst + '}]}}']
        return coap_message
    elif coap == 'high_temp_limit':
        coap_message = ['1', '1', '{"Cfg":{"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 8,"Queue": [{"Priority": 64,"SPHighTemperature": ' + config.temp_max + '}]}}']
        return coap_message
    elif coap == 'power_factor_limit':
        coap_message = ['2', '1', '{"Cfg":{"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 1,"Queue": [{"Priority": 64,"PowerFactorLimit": ' + config.PowerFactorLimit + '}]}}']
        return coap_message
    elif coap == 'voltage_hyst':
        coap_message = ['4', '1', '{"Cfg":{"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 22,"Queue": [{"Priority": 65,"VoltageTimeHyst": ' + config.VoltageTimeHyst + '}]}}']
        return coap_message
    elif coap == 'high_voltage_limit':
        coap_message = ['4', '1', '{"Cfg":{"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 22,"Queue": [{"Priority": 64,"SPHighVoltage": ' + config.SPHighVoltage + '}]}}']
        return coap_message
    elif coap == 'low_voltage_limit':
        coap_message = ['4', '1', '{"Cfg":{"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 22,"Queue": [{"Priority": 64,"SPLowVoltage": ' + config.SPLowVoltage + '}]}}']
        return coap_message
    elif coap == 'startup_seq_time':
        coap_message = ['1', '1', '{"Cfg":{"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 3,"Queue": [{"Priority":6,"StartUpSequenceTime": ' + config.start_up_seq_time + '}]}}']
        return coap_message
    elif coap == 'photocell':
        coap_message = ['1', '1', '{"Cfg":{"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 3,"Queue": [{"Priority":6,"PhotoCellEnable": ' + config.photocell_enabled + '}]}}']
        return coap_message
    elif coap == '1_10V_dim_curve':
        coap_message = ['2', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":1,"Queue":[{"Priority":64,"Dimming_curve":['
                        + config.dimcurve_dict["OversizeFactor"] + ', '
                        + config.dimcurve_dict["MaintenanceFactor"] + ', '
                        + config.dimcurve_dict["MaintenanceInterval"] + ', '
                        + config.dimcurve_dict["LowWattMinDim"] + ', '
                        + config.dimcurve_dict["LowWattMaxDim"] + ', '
                        + config.dimcurve_dict["HighWattMinDim"] + ', '
                        + config.dimcurve_dict["HighWattMaxDim"] + ', '
                        + config.dimcurve_dict["PowerFactorLimit"] + ', '
                        '"1-10V", '
                        + config.dimcurve_dict["MinVoltage"] + ', '
                        + config.dimcurve_dict["MinPower"] + ', '
                        + config.dimcurve_dict["MinLumen"] + ' '
                        + config.dimcurve_dict["MaxVoltage"] + '] } ] } }'
                        ]
        return coap_message
    elif coap == 'DALI_dim_curve':
        coap_message = ['2', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":1,"Queue":[{"Priority":64,"Dimming_curve":['
                        + config.dimcurve_dict["OversizeFactor"] + ', '
                        + config.dimcurve_dict["MaintenanceFactor"] + ', '
                        + config.dimcurve_dict["MaintenanceInterval"] + ', '
                        + config.dimcurve_dict["LowWattMinDim"] + ', '
                        + config.dimcurve_dict["LowWattMaxDim"] + ', '
                        + config.dimcurve_dict["HighWattMinDim"] + ', '
                        + config.dimcurve_dict["HighWattMaxDim"] + ', '
                        + config.dimcurve_dict["PowerFactorLimit"] + ', '
                        '"DALI", '
                        + config.dimcurve_dict["DALIindex"] + ', '
                        + config.dimcurve_dict["MinLumen"] + '] } ] } }'
                        ]
        return coap_message
    elif coap == 'def_switch_prf_empty_Q':
        coap_message = ['2', config.switch_prf_index, '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":10,"Queue":[]}}']
        return coap_message
    elif coap == 'def_switch_prf':
        coap_message = ['2', config.switch_prf_index, '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":10,"Queue":' + config.switch_prf + '}}']
        return coap_message
    elif coap == 'switch_prf_empty_Q':
        coap_message = ['2', config.switch_prf_index, '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":14,"Queue":[]}}']
        return coap_message
    elif coap == 'switch_prf':
        coap_message = ['2', config.switch_prf_index, '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":14,"Queue":[' + config.switch_prf + ']}}']
        return coap_message
    elif coap == 'man_dim_com_empty_Q':
        coap_message = ['2', config.man_dim_index, '{"Ctrl":{"UTC":"' + config.now_UTC() + '","DeviceID":""' + device_id + '"",\
        "ContID":1,"Queue":[]}}']
        return coap_message
    elif coap == 'man_dim_com':
        coap_message = ['2', config.man_dim_index, '{"Ctrl":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ContID":1,"Queue":[{"Priority":191,"LightLvl":' + config.man_dim_level + ',"Valid":{"Set":["[3.1.LocDateTime]",\
        "{' + UTC_TIME_plus_5min + '}","<="]}}]}}']  # TODO: need to make time variable. not only dim for 5minutes
        return coap_message
    elif coap == 'gps_position':
        coap_message = ['3', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID": 11,"Queue":[{"Priority":64,"GPSConfLAT":' + config.gps_lat + '},\
        {"Priority":64,"GPSConfLON":' + config.gps_long + '}]}}']
        return coap_message
    elif coap == 'timezone_config':
        coap_message = ['3', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID": 11,"Queue":[{"Priority":64,"Timezone":' + config.timezone + '}]}}']
        return coap_message
    elif coap == 'daylight_sav_config':
        coap_message = ['3', '1', '{"Cfg": {"UTC": "' + config.now_UTC() + '","DeviceID": "' + device_id + '",\
        "ConfID": 11,"Queue": [[{"Priority": 64,"DaylightSavingDelta": 1,\"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 3 28 3 * 2021}",">=","[3.1.LocDateTime]","{0 59 2 31 10 * 2021}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 27 3 * 2022}",">=","[3.1.LocDateTime]","{0 59 2 30 10 * 2022}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 26 3 * 2023}",">=","[3.1.LocDateTime]","{0 59 2 29 10 * 2023}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 31 3 * 2024}",">=","[3.1.LocDateTime]","{0 59 2 27 10 * 2024}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 30 3 * 2025}",">=","[3.1.LocDateTime]","{0 59 2 26 10 * 2025}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 29 3 * 2026}",">=","[3.1.LocDateTime]","{0 59 2 25 10 * 2026}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 28 3 * 2027}",">=","[3.1.LocDateTime]","{0 59 2 31 10 * 2027}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 26 3 * 2028}",">=","[3.1.LocDateTime]","{0 59 2 29 10 * 2028}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 25 3 * 2029}",">=","[3.1.LocDateTime]","{0 59 2 28 10 * 2029}","<","&&"]}},'
        '{"Priority": 64,"DaylightSavingDelta": 1,"Valid": {"Set": ["[3.1.LocDateTime]","{0 0 2 31 3 * 2030}",">=","[3.1.LocDateTime]","{0 59 2 27 10 * 2030}","<","&&"]}}]]}}']
        return coap_message
    # elif coap == 'rconf_trig':
    #     coap_message = ['5', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
    #     "ConfID":3,"Queue":[{"Priority":5,"DNS_ServerIP":"' + config.DNS_serverIP + '"},\
    #     {"Priority": 5,"APNconf":["' + config.rconf_apn_url + '","' + config.rconf_apn_usr + '","' + config.rconf_apn_pwd + '"]},\
    #     {"Priority": 5, "Rconf": ["' + config.rconf_url + '.1", "", ""]},\
    #     {"Priority": 5, "ReregistrationParameters": [1, 10, 15]}]}}']
    #     return coap_message
    elif coap == 'exedra_reg_server_no_trig':
        coap_message = ['5', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '",\
        "ConfID":3,"Queue":[{"Priority":5,"DNS_ServerIP":"' + config.DNS_serverIP + '"},\
        {"Priority": 5,"APNconf":["' + config.EXEDRA_APN_URL + '","' + config.EXEDRA_APN_USR + '","' + config.EXEDRA_APN_PWD + '"]},\
        {"Priority": 5, "Rconf": ["' + config.exedra_Rurl + '.1", "", ""]}]}}']
        return coap_message
    elif coap == 'exedra_reg_server':
        coap_message = ['5', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '",\
        "ConfID":3,"Queue":[{"Priority":5,"DNS_ServerIP":"' + config.DNS_serverIP + '"},\
        {"Priority": 5,"APNconf":["' + config.EXEDRA_APN_URL + '","' + config.EXEDRA_APN_USR + '","' + config.EXEDRA_APN_PWD + '"]},\
        {"Priority": 5, "Rconf": ["' + config.exedra_Rurl + '.1", "", ""]},\
        {"Priority": 5, "ReregistrationParameters": [1, 10, 15]}]}}']
        return coap_message
    elif coap == 'ow_prj_server':
        coap_message = ['5', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '",\
        "ConfID":2,"Queue":[{"Priority":64,"APNconf":["","",""]},\
        {"Priority":64,"Sconf":["lisbon-ponte-stage.owlet-iot.com","",""]},{"Priority":64,"CelOps":["auto",0]}]}}']
        return coap_message
    elif coap == 'exedra_prj_server':
        coap_message = ['5', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '",\
        "ConfID":2,"Queue":[{"Priority":64,"APNconf":["' + config.EXEDRA_APN_URL + '","' + config.EXEDRA_APN_USR + '","' + config.EXEDRA_APN_PWD + '"]},\
        {"Priority":64,"Sconf":["' + config.exedra_Surl + '.1","",""]},{"Priority":64,"CelOps":["auto",0]}]}}']
        return coap_message
    elif coap == 'exedra_upd_server':
        coap_message = ['7', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '",\
        "ConfID": 6,"Queue": [{"Priority": 64,"UpdInf": ["' + config.EXEDRA_APN_URL + '", "' + config.EXEDRA_APN_USR + '", "' + config.EXEDRA_APN_PWD + '",\
        "http://' + config.exedra_Rurl + '.2:8001/' + config.EXEDRA_FW_UPD_SERVER + '", "", ""]}]}}']
        return coap_message
    elif coap == 'exedra_upd_notif_server':
        coap_message = ['7', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '",\
        "ConfID":6,"Queue":[{"Priority":64,"UpdateNotifServer":\
        ["http://' + config.exedra_Rurl + '.2:8001/' + config.EXEDRA_FW_UPD_NOTIF_SERVER + '"]}]}}']
        return coap_message
    elif coap == 'trig_upd':
        coap_message = ['7', '1', '{"Ctrl":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ContID":8,"Queue":[{"Priority":128,"UpdateApp":"' + config.fw_update + '"}]}}']
        return coap_message
    elif coap == 'trig_dwgd':
        coap_message = ['7', '1', '{"Ctrl":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ContID":8,"Queue":[{"Priority":128,"UpdateApp":"' + config.fw_current + '"}]}}']
        return coap_message
    elif coap == 'sensor_cfg':
        coap_message = ['8', '1', '{"Cfg":{"UTC":"' + config.now_UTC() + '","DeviceID":"' + device_id + '",\
        "ConfID":13,"Queue":[{"Priority": 64,"SensorGeneral": ["' + config.source_device_id + '","' + config.sensor_type + '",0,' + config.sensor_hold_time + ',' + config.sup_dim_feed + ']}]}}']
        return coap_message
    else:
        logger.warning(f'Unknown C2D Coap message: {coap}')
        coap_message = []
        return coap_message








