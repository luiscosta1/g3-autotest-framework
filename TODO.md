# TODO

## Prio 1
* need to implement the follow-through even in case a step fails! Otherwise we won't disconnect from the device and next test connection will fail!
* Use retry mechanism on Device communication
* Create a config variable for the debug level. add it to the UI
* Create  function to call `os.system('cmd /c "behave"')` from the main ExedraG3AutomationTool - Done. Need to implement in the UI
## Prio 2
* Create a different logger for the main app only, to separate between Tool Logs and Execution logs
* Use the before_all(context) to setup anything needed while testing:
* *   example in https://behave.readthedocs.io/en/stable/practical_tips.html
* Document this framework in Confluence
* Integrate Selenium Steps in steps_exedra_ui
## Prio 3
* Put Local CLi to work


Knowledge sharing Ideas:
logs.py: remove dependency on the existence of the logfile.log prior to execution of the tests
logfile named after the test execution  being run (1 log file per test execution)

Use katalon for Manual Commands on fully end-to-end tests.