# README 

This README documents the necessary steps are to get your application up and running.

More information can be found in this project's Confluence Page:
https://owlet-de.atlassian.net/wiki/spaces/CTT/pages/2715058177/Exedra+G3+Automation+Tool

## Requirements for developing this tool

To setup a development environment for this tool, the following requirements must be met:
* Python 3.6 or newer  
* EEL Library for the Web UI - `pip install Eel`
* HTTP Requests library to access Imperium or Exedra APIs - `pip install requests`
* Telnet library to communicate with a SeCo - `pip install telnet2`
* Coapthon3 library to send D2C messages - `pip install coapthon3`
* Serial library to communicate directly to the controllers - `pip install serial`
* Easy Settings Library for saving and loading the settings - `pip install EasySettings`
* Py Installer Library to create the executable file - `pip install pyinstaller`
* (optional) PyTest Library for Unit Testing to the code - `pip install pytest`
* Retry Mechanism - `pip install retry`
* Behave library, to process the testcases in Gherkin language - `pip install behave`
* Selenium library, for test steps that require access to the Web UI - `pip install selenium`

The requirements can be all installed at once, by running `pip install -r requirements.txt`

Sometimes, the wrong library for serial is installed. use:  
`pip install --upgrade --force-reinstall pyserial`

### Before Testing

First you need to add a file "credential.json" to a folder called "credential" that is at the same as the main project. In the file add the following:

`Replace the data with yours XRAY and JIRA client and tokens.`
````json
{
    "xray":{
                "user": "XRAY_CLIENT_ID",
                "token":"XRAY_CLIENT_SECRET"
            }, 
    "jira":{
                "user":"CLIENT_EMAIL",
                "token":"CLIENT_TOKEN"
            }
}   
````

The project file organization should look like this:

```bash
C:.
├───Auto-Test_Framework
│   ├───anx  					(Any annex from a test will be downloaded to this folder)
│   ├───connect 				(Connection scripts for both Serial COM and Secure Shell)
│   ├───features 				(Cucumber tests exported from XRAY will be downloaded in this folder)
│   ├───logs 					(Log files from the executed tests are located in this folder)
│   ├───results 				(results from the tests are kept in this folder in *.json file that will be imported into XRAY)
│   ├───steps 					(Scripts that identify and executes the Given, When and Then statements from Cucumber files)
│   ├───support 				(Support Scripts, i.e. logging and beauty scripts)
│   └───xrayAPI 				(Scripts to import\export tests to XRAY plateform)
├───credentials 				(json file containing the user/tokens for both jira and XRAY)
```

### Setting up the test parameters

Run the main application via `python main.py` to run the Exedra G3 Automation Tool and setup all the testing parameters, such as the Device IDs, or SeCo address

### Testing

Run the "run_tests.py -h" script to check the available features. The script is designed to run "Test Executions" issues from XRAY test managment platform.

Example using the script to test the NGF-3261 "Test Execution":

`run_tests.py --k TST-2899 -e -l --loglevel INFO`

### Building a Distributable package  ###
To create a single-file application, the following command shall be used:

* pyinstaller main.py --add-data "gui;gui" --name ExedraG3AutomationTool_[version] --onefile --icon icon.ico --clean


### Who do I talk to? ###

* Luís Costa <lcosta@schreder.com>
* Luís Gonçalves <lugoncalves@schreder.com>