####################################################################################
# Filename:     config.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Version:      2.0
#
# Filetype:     Config file
# Description:  Runtime variables for Exedra G3 Automation Tool
# STATUS:       New
# Limitation:   This file needs to be in the project root folder. It's the master of all runtime parameters
####################################################################################
"""Runtime variables for Exedra G3 Automation Tool"""

import time
from easysettings import JSONSettings
from logs import logger


########### Environment Global Variables ##############
imported_devices_list = []

NUM_THREADS = 5
SETTINGS_FILENAME = 'settings.json'

########### G3 Serial Port Settings ###################
cli_port = 'COM16'
cli_logs_port = 'COM20'
cli_binary_port = 'COM18'
cli_baudrate = 115200
cli_databits = 8
cli_stopbits = 1
cli_parity = 'None'
cli_timeout = 1

serial_logs_max_characters = 500
#######################################################
# True:  using SeCo
# False: using local USB connection
g3_cli_remote = True

############### G3 SeCo Settings ######################
# these may change. loaded from settings.json file
seco_host = "hyperiontest.ddns.net"
seco_port = "60913"
seco_timeout = 10
# these never change. used as constants
SECO_USER = "root"
SECO_PASSWORD = "nightshift"

#######################################################
# TODO: Delete above and use dict below

############### G3 Settings (SeCo + Local CLI)######################

g3_connection_settings = {
    "g3_cli_remote": True,
    "seco_host": "hyperiontest.ddns.net",
    "seco_port": "60913",
    "seco_timeout": 10,
    "seco_user": "root",
    "seco_password": "nightshift",
    "cli_port": "COM16",
    "cli_logs_port": "COM20",
    "cli_binary_port": "COM18",
    "cli_baudrate": 115200,
    "cli_databits": 8,
    "cli_stopbits": 1,
    "cli_parity": 'None',
    "cli_timeout": 1
}

###### Exedra-Specific ######
EXEDRA_APN_URL = 'global.we.vp'
EXEDRA_APN_USR = 'orange'
EXEDRA_APN_PWD = 'orange'

EXEDRA_FW_UPD_SERVER = 'api/v1/G3/fw'
EXEDRA_FW_UPD_NOTIF_SERVER = 'api/v1/G3/fw'

# Reg Server Settings
EXEDRA_RURL_DEV		= '10.100.162'
EXEDRA_RURL_TST		= '10.100.98'
EXEDRA_RURL_PRD		= '10.100.34'

# Project Server Settings
EXEDRA_SURL_DEV		= '10.100.162'
EXEDRA_SURL_TST		= '10.100.98'
EXEDRA_SURL_PRD		= '10.100.34'

# Exedra URLs:
EXEDRA_URL_DEV = 'hyperion.dev-schreder-exedra.com'
EXEDRA_URL_TST = 'hyperion.tst-schreder-exedra.com'
EXEDRA_URL_PRD = 'hyperion.schreder-exedra.com'

# Exedra Bearer Tokens: Token Must be created in Exedra -> Settings -> Personal access Tokens
EXEDRA_TOKEN_DEV = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNjA4ZmI2YTg1NjBkMDMwMDIxYjlmMDcyIiwidGVuYW50IjoiSHlwZXJpb24iLCJ0b2tlbklkIjoiYzU3ZTUwYTAtMzUyNS00OGFiLTk1ZTMtZWMwZmNiMDQ1ZDhhIiwiaWF0IjoxNjM1Njk0MjY5fQ.giMnSIzBw5day6CvT22KZLJF9pHJywLCkWz8R9u00EY'
EXEDRA_TOKEN_TST = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWY5ZmZjY2ZkZDAyNGMwMDFlYmI2MzE3IiwidGVuYW50IjoiSHlwZXJpb24iLCJ0b2tlbklkIjoiNDQ0MWRjYjgtMjllOC00ZTAzLTg1NTgtZGQwNjJlZDY1OWUyIiwiaWF0IjoxNjA3MzY2MDM4fQ.7OHYbE15seAO1tezfgVoZ9l5eqG0_l_BjEbdT2ABZ9E'
EXEDRA_TOKEN_PRD = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWY5ZmZkYTY4MjA1YzcwMDFmZWEyNjZlIiwidGVuYW50IjoiSHlwZXJpb24iLCJ0b2tlbklkIjoiOGIzMDNkN2MtNmEzOC00MWIzLThjMzktNDczYWNkYjM3NTAzIiwiaWF0IjoxNjI5MjA0MTExfQ.mOU5kZ8sh9GRloyvySSqP6lIhbDOae_n7LoSBBMKVu0'

##############################

# Device Management API Settings
DEVICE_MANAGEMENT_URL_DEV   = 'hyp-shared-dev-we-device-management-api.azurewebsites.net'
DEVICE_MANAGEMENT_URL_TST   = 'hyp-shared-tst-we-device-management-api.azurewebsites.net'
DEVICE_MANAGEMENT_URL_PRD   = 'hyp-shared-prd-we-device-management-api.azurewebsites.net'

IOT_HUB_DEV   = 'hyp-shared-dev-we-iot-hub-01'
IOT_HUB_TST   = 'hyp-shared-tst-we-iot-hub-01'
IOT_HUB_PRD   = 'hyp-shared-prd-we-iot-hub-01'

# Azure DM API Settings
AZURE_GET_DM_API_CREDENTIALS_DEV = {
    "grant_type": "client_credentials",
    "client_id": "39039fc4-c20d-4305-8ee0-b707d8182c6d",
    "client_secret": "XlE7Q~.6x4GtDd_nRaXXqsxnkgx-PCDAOrL98",
    "scope": "api://11297982-ed81-4898-a0a8-bb19346b4c4f/.default"
}

AZURE_GET_DM_API_CREDENTIALS_TST = {
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://51aaa53c-893b-4777-a076-01a1dbd3a284/.default"
}

AZURE_GET_DM_API_CREDENTIALS_PRD = {
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://11297982-ed81-4898-a0a8-bb19346b4c4f/.default"
}

device_management_bearer_token = ''
device_management_bearer_token_validity = time.time()

##############################

# Asset Management API Settings
ASSET_MANAGEMENT_API_URL_DEV   = 'hyp-shared-dev-we-api-gateway.azure-api.net'
ASSET_MANAGEMENT_API_URL_TST   = 'hyp-shared-tst-we-api-gateway.azure-api.net'
ASSET_MANAGEMENT_API_URL_PRD   = 'hyp-shared-prd-we-api-gateway.azure-api.net'

# Azure Asset Management API Settings
AZURE_GET_AM_API_CREDENTIALS_DEV = {
    "grant_type": "client_credentials",
    "client_id": "39039fc4-c20d-4305-8ee0-b707d8182c6d",
    "client_secret": "XlE7Q~.6x4GtDd_nRaXXqsxnkgx-PCDAOrL98",
    "scope": "api://601507e2-018f-4b92-ae06-7061080c4c96/.default"
}

AZURE_GET_AM_API_CREDENTIALS_TST = {
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://62f7e149-5c3d-4080-94fc-bda5af73894e/.default"
}

AZURE_GET_AM_API_CREDENTIALS_PRD = { # Not sure if these match, but let's not use PROD environment for tests
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://11297982-ed81-4898-a0a8-bb19346b4c4f/.default"
}

asset_management_bearer_token = ''
asset_management_bearer_token_validity = time.time()

#######################################################

# Tenant Management API Settings
TENANT_MANAGEMENT_API_URL_DEV   = 'hyp-shared-dev-we-api-gateway.azure-api.net'
TENANT_MANAGEMENT_API_URL_TST   = 'hyp-shared-tst-we-api-gateway.azure-api.net'
TENANT_MANAGEMENT_API_URL_PRD   = 'hyp-shared-prd-we-api-gateway.azure-api.net'

# Azure Tenant Management API Settings
AZURE_GET_TM_API_CREDENTIALS_DEV = {
    "grant_type": "client_credentials",
    "client_id": "39039fc4-c20d-4305-8ee0-b707d8182c6d",
    "client_secret": "XlE7Q~.6x4GtDd_nRaXXqsxnkgx-PCDAOrL98",
    "scope": "api://0971e0da-392e-4f8c-891b-3db7724e8fb6/.default"
}

AZURE_GET_TM_API_CREDENTIALS_TST = {
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://4558a922-bb04-4e86-911c-a921c759e184/.default"
}

AZURE_GET_TM_API_CREDENTIALS_PRD = { # Not sure if these match, but let's not use PROD environment for tests
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://11297982-ed81-4898-a0a8-bb19346b4c4f/.default"
}

tenant_management_bearer_token = ''
tenant_management_bearer_token_validity = time.time()
#######################################################

# Firmware Management API Settings
FIRMWARE_MANAGEMENT_API_URL_DEV   = 'hyp-shared-dev-we-api-gateway.azure-api.net'
FIRMWARE_MANAGEMENT_API_URL_TST   = 'hyp-shared-tst-we-api-gateway.azure-api.net'
FIRMWARE_MANAGEMENT_API_URL_PRD   = 'hyp-shared-prd-we-api-gateway.azure-api.net'

# Azure Tenant Management API Settings
AZURE_GET_FW_API_CREDENTIALS_DEV = {
    "grant_type": "client_credentials",
    "client_id": "39039fc4-c20d-4305-8ee0-b707d8182c6d",
    "client_secret": "XlE7Q~.6x4GtDd_nRaXXqsxnkgx-PCDAOrL98",
    "scope": "api://ba0d9109-96c2-4213-b085-7f0a3ba2da8a/.default"
}

AZURE_GET_FW_API_CREDENTIALS_TST = {
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://718c9eb4-adc3-4b95-906c-907346f00877/.default"
}

AZURE_GET_FW_API_CREDENTIALS_PRD = { # Not sure if these match, but let's not use PROD environment for tests
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://11297982-ed81-4898-a0a8-bb19346b4c4f/.default"
}

firmware_management_bearer_token = ''
firmware_management_bearer_token_validity = time.time()

#######################################################

# G3 Public Gateway Settings
G3_GATEWAY_HOST_TST = '40.74.34.91'  # TST
G3_GATEWAY_HOST_DEV = '20.86.160.213'  # DEV
G3_GATEWAY_HOST_PRD = ''  # Not Available

#######################################################

# Set the initial value of the runtime variables
# Default Environment shall be TST
# If a valid settings.json file is found, these values will be overwritten by load_settings()

environment = 'TST'
g3_gateway_host = G3_GATEWAY_HOST_TST

# Device Management API
device_management_url = DEVICE_MANAGEMENT_URL_TST
iot_hub = IOT_HUB_TST
azure_get_dm_api_credentials = AZURE_GET_DM_API_CREDENTIALS_TST

# Asset Management API
asset_api_url = ASSET_MANAGEMENT_API_URL_TST
azure_get_am_api_credentials = AZURE_GET_AM_API_CREDENTIALS_TST

# Tenant Management API
tenant_api_url = TENANT_MANAGEMENT_API_URL_TST
azure_get_tm_api_credentials = AZURE_GET_TM_API_CREDENTIALS_TST

# Firmware Management API
firmware_api_url = FIRMWARE_MANAGEMENT_API_URL_TST
azure_get_fw_api_credentials = AZURE_GET_FW_API_CREDENTIALS_TST

fw_upd_server = EXEDRA_FW_UPD_SERVER
fw_upd_notif_server = EXEDRA_FW_UPD_NOTIF_SERVER

exedra_url = EXEDRA_URL_TST
exedra_Rurl = EXEDRA_RURL_TST
exedra_Surl = EXEDRA_SURL_TST
exedra_token = EXEDRA_TOKEN_TST
#######################################################


########### Runtime Global Variables ##################
device_dummy = '0013A200416CFA5A'
device_1_10v = '0013A20041BB0358'
device_dali = '0013A20041BC6DF1'
device_multi_dali = '0013A200YYYYYYYY'
device_tw = '0013A20041BB15DE'

DeviceID = device_dali

AssetID = 'E00401009F5A3E6E'

fw_current = '3.32.15.100'
fw_update = '3.32.15.101'
fw_illegal = '3.32.15.101.fake'

tenant = 'Nova-Lab'
gps_nova_sbe_lat = '38.67824'
gps_nova_sbe_long = '-9.32672'

gps_lat = gps_nova_sbe_lat
gps_long = gps_nova_sbe_long

# Sensor configuration
source_device_id = '0000000000000000'
sensor_type = 'local'
sensor_hold_time = '90'
sup_dim_feed = 'true'

# Dimfeedback values
dimfeedback_dict = {
    "EnergyMeter": "270",
    "ACPower": "89",
    "ACCurrent": "0.300",
    "ACVoltage": "240",
    "ACPowerFactor": "0.95",
    "FeedbackDIMLevel": "20",
    "MinACPower": "85",
    "MaxACPower": "90"
}

# BurningHours values
BurningHours_dict = {
    "TotalControllerRuntime": "2500",
    "TotalRuntime": "2000"  # Lamp 1
}

# ACPower Errors List
ACPower_errors_list = ['ErrPWRPF', 'ErrVoltageHigh', 'ErrZXMissing',
                       'ErrPWRHigh', 'ErrVoltageLow', 'ErrSAGDetected', 'ErrPWRLow']

# ACPower Errors State
ACPower_errors_dict = {
    "ErrPWRPF": True,
    "ErrVoltageHigh": True,
    "ErrZXMissing": True,
    "ErrPWRHigh": True,
    "ErrVoltageLow": True,
    "ErrSAGDetected": True,
    "ErrPWRLow": True,
    "ErrNoLoad": True
}

BrokenLamp = False

DALIBallastStatus = "LampArcPowerOn"  # "LampArcPowerOn" OR "CommsError"
DALI101_Error = 0

DALIStatus = ""

ErrIPCComm = False
ErrTemperatureHigh = False

DNS_serverIP = '195.177.188.21'

lamp_type = '1-10V'

man_dim_level = '100'
man_dim_index = '1'

switch_prf_index = '1'
switch_prf = '[{"Priority":64,"LightLvl":100},{"Priority":67,"LightLvl":0,"Valid":{"Set":["[1.1.AmbientLightStatus]"]}}]'

timezone = '0'
daylight_saving_delta = '1'

burn_hour_timeout = '1440'
temp_time_hyst = '10'
temp_max = '70'
start_up_seq_time = '960'
photocell_enabled = 'true'
sensor_count = '10'
PowerFactorLimit = "0.5"
VoltageTimeHyst = "10"
SPHighVoltage = "300"
SPLowVoltage = "200"


# Dimming Curve values
dimcurve_dict = {
    "OversizeFactor": "100",
    "MaintenanceFactor": "100",
    "MaintenanceInterval": "100000",
    "LowWattMinDim": "1",
    "LowWattMaxDim": "15",
    "HighWattMinDim": "10",
    "HighWattMaxDim": "40",
    "PowerFactorLimit": "0.5",
    "LampTypeIP": '"1-10V"',
    "DALIindex": "1",        # DALI Only
    "MinVoltage": "1",       # 1-10V Only
    "MinPower": "1",         # 1-10V Only
    "MinLumen": "10",        # DALI + 1-10V
    "MaxVoltage": "10"       # 1-10V Only
}

#######################################################

# Create settings file instance, but don't try to open the file.
settings = JSONSettings()


# Current Time, in different formats
def now_UTC():
    """
    This function returns the current time in the G3 payload format YYMMDDhhmmss
    """
    return time.strftime("%y%m%d%H%M%S")


def now_Time():
    """
    This function returns the current time in the Rabbit MQ format e.g.: 2021-08-09T16:24:39.000Z
    """
    return time.strftime("%Y-%m-%dT%H:%M:%S.000Z")


def load_settings():
    """
    This function loads the settings from the config file settings.json
    These settings are, then, synched to the config.py runtime variables
    """
    # Settings to be loaded from the Settings file
    global settings

    # The Global variables above, with pre-defined values, which may be changed in Runtime
    # TODO: remove cli_port, seco_host, seco_port, g3_cli_remote as they are now inside "g3_connection_settings"
    global DeviceID, AssetID, fw_illegal, fw_update, gps_lat, gps_long, \
        dimfeedback_dict, BurningHours_dict, ACPower_errors_dict, BrokenLamp, DNS_serverIP, \
        lamp_type, dimcurve_dict, fw_upd_server,\
        fw_upd_notif_server, exedra_Rurl, exedra_Surl, man_dim_level, man_dim_index, switch_prf_index, timezone,\
        burn_hour_timeout, temp_time_hyst, temp_max, start_up_seq_time, photocell_enabled, DALIBallastStatus,\
        sensor_count, g3_gateway_host, environment, daylight_saving_delta, device_dummy, device_1_10v, device_dali, \
        device_multi_dali, device_tw, g3_connection_settings, ErrIPCComm, ErrTemperatureHigh, DALIStatus, DALI101_Error,\
        PowerFactorLimit, VoltageTimeHyst, SPHighVoltage, SPLowVoltage

    try:
        # Let's first try and load the settings saved in "settings.json"
        settings = settings.from_file(SETTINGS_FILENAME)
        logger.debug(f'Found the settings file: {SETTINGS_FILENAME}')
    except FileNotFoundError:
        # File not found... Create it and save the default settings in config.py into it
        logger.warning(f'Settings file {SETTINGS_FILENAME} not found. Creating a new one...')
        settings.filename = SETTINGS_FILENAME
        create_default_settings()

    try:
        DeviceID = settings.get("DeviceID")
        device_dummy = settings.get("Dummy")
        device_1_10v = settings.get("1-10V")
        device_dali = settings.get("DALI")
        device_multi_dali = settings.get("Multi-DALI")
        device_tw = settings.get("Tuneable-White")
        AssetID = settings.get("AssetID")

        g3_connection_settings = settings.get("g3_connection_settings")

        fw_illegal = settings.get("fw_illegal")
        fw_update = settings.get("fw_update")
        gps_lat = settings.get("gps_lat")
        gps_long = settings.get("gps_long")

        dimfeedback_dict = settings.get("dimfeedback_dict")
        BurningHours_dict = settings.get("BurningHours_dict")
        ACPower_errors_dict = settings.get("ACPower_errors_dict")
        BrokenLamp = settings.get("BrokenLamp")
        ErrIPCComm = settings.get("ErrIPCComm")
        ErrTemperatureHigh = settings.get("ErrTemperatureHigh")
        DALIStatus = settings.get("DALIStatus")
        DALIBallastStatus = settings.get("DALIBallastStatus")
        DALI101_Error = settings.get("DALI101_Error")

        DNS_serverIP = settings.get("DNS_serverIP")

        lamp_type = settings.get("lamp_type")
        dimcurve_dict = settings.get("dimcurve_dict")

        fw_upd_server = settings.get("fw_upd_server")
        fw_upd_notif_server = settings.get("fw_upd_notif_server")

        exedra_Rurl = settings.get("exedra_Rurl")
        exedra_Surl = settings.get("exedra_Surl")

        man_dim_level = settings.get("man_dim_level")
        man_dim_index = settings.get("man_dim_index")

        switch_prf_index = settings.get("switch_prf_index")
        timezone = settings.get("timezone")
        burn_hour_timeout = settings.get("burn_hour_timeout")
        temp_time_hyst = settings.get("temp_time_hyst")
        temp_max = settings.get("temp_max")
        start_up_seq_time = settings.get("start_up_seq_time")
        photocell_enabled = settings.get("photocell_enabled")

        daylight_saving_delta = settings.get("daylight_saving_delta")
        sensor_count = settings.get("sensor_count")
        PowerFactorLimit = settings.get("PowerFactorLimit")
        VoltageTimeHyst = settings.get("VoltageTimeHyst")
        SPHighVoltage = settings.get("SPHighVoltage")
        SPLowVoltage = settings.get("SPLowVoltage")

        g3_gateway_host = settings.get("g3_gateway_host")
        environment = settings.get("environment")

        set_environment(environment)

    except KeyError:
        # Something is wrong with the file or some setting was not found. Create new settings file
        logger.warning("Error Loading parameters from settings file. Creating new settings file")
        create_default_settings()

    return True


def set_environment(env):

    global environment, exedra_url, exedra_Rurl, exedra_Surl, exedra_token,\
        iot_hub, device_management_url, azure_get_dm_api_credentials, g3_gateway_host, asset_api_url, \
        azure_get_am_api_credentials, tenant_api_url, azure_get_tm_api_credentials, firmware_api_url, \
        azure_get_fw_api_credentials

    if env == "DEV":
        logger.info(f'Setting Environment to {environment}')

        environment = env
        # Exedra
        exedra_url = EXEDRA_URL_DEV
        exedra_Rurl = EXEDRA_RURL_DEV
        exedra_Surl = EXEDRA_SURL_DEV
        exedra_token = EXEDRA_TOKEN_DEV
        # Imperium
        ### Device Management API
        iot_hub = IOT_HUB_DEV
        device_management_url = DEVICE_MANAGEMENT_URL_DEV
        azure_get_dm_api_credentials = AZURE_GET_DM_API_CREDENTIALS_DEV
        ### Asset Management API
        asset_api_url = ASSET_MANAGEMENT_API_URL_DEV
        azure_get_am_api_credentials = AZURE_GET_AM_API_CREDENTIALS_DEV
        ### Tenant Management API
        tenant_api_url = TENANT_MANAGEMENT_API_URL_DEV
        azure_get_tm_api_credentials = AZURE_GET_TM_API_CREDENTIALS_DEV
        ### Firmware Management API
        firmware_api_url = FIRMWARE_MANAGEMENT_API_URL_DEV
        azure_get_fw_api_credentials = AZURE_GET_FW_API_CREDENTIALS_DEV
        # G3 Public Gateway
        g3_gateway_host = G3_GATEWAY_HOST_DEV
        return True

    elif env == "TST":
        logger.info(f'Setting Environment to {environment}')

        environment = env
        #Exedra
        exedra_url = EXEDRA_URL_TST
        exedra_Rurl = EXEDRA_RURL_TST
        exedra_Surl = EXEDRA_SURL_TST
        exedra_token = EXEDRA_TOKEN_TST
        # Imperium
        ### Device Management API
        iot_hub = IOT_HUB_TST
        device_management_url = DEVICE_MANAGEMENT_URL_TST
        azure_get_dm_api_credentials = AZURE_GET_DM_API_CREDENTIALS_TST
        ### Asset Management API
        asset_api_url = ASSET_MANAGEMENT_API_URL_TST
        azure_get_am_api_credentials = AZURE_GET_AM_API_CREDENTIALS_TST
        ### Tenant Management API
        tenant_api_url = TENANT_MANAGEMENT_API_URL_TST
        azure_get_tm_api_credentials = AZURE_GET_TM_API_CREDENTIALS_TST
        ### Firmware Management API
        firmware_api_url = FIRMWARE_MANAGEMENT_API_URL_TST
        azure_get_fw_api_credentials = AZURE_GET_FW_API_CREDENTIALS_TST
        # G3 Public Gateway
        g3_gateway_host = G3_GATEWAY_HOST_TST
        return True

    elif env == "PRD":
        logger.info(f'Setting Environment to {environment}')

        environment = env
        # Exedra
        exedra_url = EXEDRA_URL_PRD
        exedra_Rurl = EXEDRA_RURL_PRD
        exedra_Surl = EXEDRA_SURL_PRD
        exedra_token = EXEDRA_TOKEN_PRD
        # Imperium
        iot_hub = IOT_HUB_PRD
        device_management_url = DEVICE_MANAGEMENT_URL_PRD
        azure_get_dm_api_credentials = AZURE_GET_DM_API_CREDENTIALS_PRD
        # Asset Management API
        asset_api_url = ASSET_MANAGEMENT_API_URL_PRD
        azure_get_am_api_credentials = AZURE_GET_AM_API_CREDENTIALS_PRD
        ### Tenant Management API
        tenant_api_url = TENANT_MANAGEMENT_API_URL_PRD
        azure_get_tm_api_credentials = AZURE_GET_TM_API_CREDENTIALS_PRD
        ### Firmware Management API
        firmware_api_url = FIRMWARE_MANAGEMENT_API_URL_PRD
        azure_get_fw_api_credentials = AZURE_GET_FW_API_CREDENTIALS_PRD
        # G3 Public Gateway
        g3_gateway_host = G3_GATEWAY_HOST_PRD
        return True

    else:
        logger.error(f'Unknown Environment: {environment}')
        return False


def create_default_settings():
    """
    This function shall be called when
        - NO settings file is found
        - Error importing Attributes from file
    It creates the default settings from the ones defined in config.py
    New file is created with the .save() method
    """

    settings.set("DeviceID", DeviceID)
    settings.set("Dummy", device_dummy)
    settings.set("1-10V", device_1_10v)
    settings.set("DALI", device_dali)
    settings.set("Multi-DALI", device_multi_dali)
    settings.set("Tuneable-White", device_tw)
    settings.set("AssetID", AssetID)

    settings.set("g3_connection_settings", g3_connection_settings)

    settings.set("fw_illegal", fw_illegal)
    settings.set("fw_update", fw_update)
    settings.set("gps_lat", gps_lat)
    settings.set("gps_long", gps_long)

    settings.set("dimfeedback_dict", dimfeedback_dict)
    settings.set("BurningHours_dict", BurningHours_dict)
    settings.set("ACPower_errors_dict", ACPower_errors_dict)
    settings.set("BrokenLamp", BrokenLamp)
    settings.set("ErrIPCComm", ErrIPCComm)
    settings.set("ErrTemperatureHigh", ErrTemperatureHigh)
    settings.set("DALIStatus", DALIStatus)
    settings.set("DALIBallastStatus", DALIBallastStatus)
    settings.set("DALI101_Error", DALI101_Error)

    settings.set("DNS_serverIP", DNS_serverIP)

    settings.set("lamp_type", lamp_type)
    settings.set("dimcurve_dict", dimcurve_dict)

    settings.set("fw_upd_server", fw_upd_server)
    settings.set("fw_upd_notif_server", fw_upd_notif_server)

    settings.set("exedra_Rurl", exedra_Rurl)
    settings.set("exedra_Surl", exedra_Surl)

    settings.set("man_dim_level", man_dim_level)
    settings.set("man_dim_index", man_dim_index)

    settings.set("switch_prf_index", switch_prf_index)
    settings.set("timezone", timezone)
    settings.set("burn_hour_timeout", burn_hour_timeout)
    settings.set("temp_time_hyst", temp_time_hyst)
    settings.set("temp_max", temp_max)
    settings.set("start_up_seq_time", start_up_seq_time)
    settings.set("photocell_enabled", photocell_enabled)
    settings.set("daylight_saving_delta", daylight_saving_delta)
    settings.set("sensor_count", sensor_count)
    settings.set("PowerFactorLimit", PowerFactorLimit)
    settings.set("VoltageTimeHyst", VoltageTimeHyst)
    settings.set("SPHighVoltage", SPHighVoltage)
    settings.set("SPLowVoltage", SPLowVoltage)

    settings.set("g3_gateway_host", g3_gateway_host)
    settings.set("environment", environment)

    try:
        settings.save()
    except EnvironmentError:
        logger.error(" Not possible to save or create settings file")
        return False

    return True


def save_current_settings():
    """
    This function saves the current environment settings to the settings.json file
    Returns True if save is successful
    Returns False if an error occurred
    """

    # Set "settings" with current runtime values
    settings.set("DeviceID", DeviceID)
    settings.set("Dummy", device_dummy)
    settings.set("1-10V", device_1_10v)
    settings.set("DALI", device_dali)
    settings.set("Multi-DALI", device_multi_dali)
    settings.set("Tuneable-White", device_tw)
    settings.set("AssetID", AssetID)

    settings.set("g3_connection_settings", g3_connection_settings)

    settings.set("fw_illegal", fw_illegal)
    settings.set("fw_update", fw_update)
    settings.set("gps_lat", gps_lat)
    settings.set("gps_long", gps_long)

    settings.set("dimfeedback_dict", dimfeedback_dict)
    settings.set("BurningHours_dict", BurningHours_dict)
    settings.set("ACPower_errors_dict", ACPower_errors_dict)
    settings.set("BrokenLamp", BrokenLamp)
    settings.set("ErrIPCComm", ErrIPCComm)
    settings.set("ErrTemperatureHigh", ErrTemperatureHigh)
    settings.set("DALIStatus", DALIStatus)
    settings.set("DALIBallastStatus", DALIBallastStatus)
    settings.set("DALI101_Error", DALI101_Error)
    settings.set("DNS_serverIP", DNS_serverIP)

    settings.set("lamp_type", lamp_type)
    settings.set("dimcurve_dict", dimcurve_dict)

    settings.set("fw_upd_server", fw_upd_server)
    settings.set("fw_upd_notif_server", fw_upd_notif_server)

    settings.set("exedra_Rurl", exedra_Rurl)
    settings.set("exedra_Surl", exedra_Surl)

    settings.set("man_dim_level", man_dim_level)
    settings.set("man_dim_index", man_dim_index)

    settings.set("switch_prf_index", switch_prf_index)
    settings.set("timezone", timezone)
    settings.set("burn_hour_timeout", burn_hour_timeout)
    settings.set("temp_time_hyst", temp_time_hyst)
    settings.set("temp_max", temp_max)
    settings.set("start_up_seq_time", start_up_seq_time)
    settings.set("photocell_enabled", photocell_enabled)
    settings.set("daylight_saving_delta", daylight_saving_delta)
    settings.set("sensor_count", sensor_count)
    settings.set("PowerFactorLimit", PowerFactorLimit)
    settings.set("VoltageTimeHyst", VoltageTimeHyst)
    settings.set("SPHighVoltage", SPHighVoltage)
    settings.set("SPLowVoltage", SPLowVoltage)

    settings.set("g3_gateway_host", g3_gateway_host)
    settings.set("environment", environment)

    # Save "settings" to file
    try:
        settings.save()
    except EnvironmentError:
        logger.error(" Not possible to save or create settings file")
        return False

    return True


# when this file is imported, load the settings from the settings.json file
load_settings()


